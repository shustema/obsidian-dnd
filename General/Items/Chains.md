#item 
#mundane 
## Common Uses
 - bait for rust monster (use ball bearings if you can)
 - stronger than rope, tie an enemy up with it
 - use with manicles to move multiple prisoners
 - use with a padlock to lock a gate or something
 - attatch to a 10-foot pole, to extend reach to almost 20 foot, to drag along the ground and avoid traps and similar
 - connect a chain to a rope if it needs to be attatched to a sharp thing to keep the rope from getting cut through
 - can use with a block and tackle to lift really heavy things
## Fun Uses
- improvised weapon/armor
	- Wrap it around your arm as an improvised shield
 - connect to a rope or stringline to jingle as an alarm system (or use a bell)
 - use as a fishing weight for a huge fish
 - good diving belt for sinking fast