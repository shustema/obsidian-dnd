#item
#mundane
#essentials
## Common Uses
- jam a lock
- create difficult terrain at chokepoints, doorways, or stairs
- jam there into the corner of a door to potentially stop the door open [[Pitons]] are better if you have them
- distract rust monsters with them. Spread them wide so you split the monsters up
- heat or cool them, and drop them into a drink
- cast heat metal on them, and create a flameless heat source (can also throw them or mage hand them at someone)
- cast darkness on one of these, and put it in your mouth. Take your turn in darkness, and then close your mouth at the end of your turn to get advantage, but not fuck your team
- cast light on them, and toss them into a room to see things
## Uncommon Uses
- teleport a ball bearing into the brain of a monster
- paint them, and sell them as everlasting gobstoppers
- can move things on a hard surface (maybe, there are a lot of caviats)
- counterbalance for a scale
- use as a source of metal for a gun, or forge cleric or something
## Etc
Take a [[Magnet]] along with the ball bearings, so you can pick them back up after using them