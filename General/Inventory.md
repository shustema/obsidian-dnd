## Essential Items
```dataview
LIST FROM #item and #essentials
SORT file.name ASC
```
## Useful Mundane Items
```dataview
LIST FROM #item and #mundane
SORT file.name ASC
```