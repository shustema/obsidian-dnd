---
campaign: <% tp.file.path(true).split("/")[1].toLowerCase().replace(/\s+/g, '-') %>
type: extra
description: 
---

```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```