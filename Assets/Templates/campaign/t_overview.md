
## Campaign Overview
## Open Loops
![[Campaigns/<% tp.file.path(true).split("/")[1].toLowerCase() %>/Quests]]
![[Campaigns/<% tp.file.path(true).split("/")[1].toLowerCase() %>/Agenda]]

## Sessions
```dataview
LIST 
FROM #<% tp.file.path(true).split("/")[1].toLowerCase() %> and #session-notes 
SORT file.name ASC
```

![[Campaigns/<% tp.file.path(true).split("/")[1].toLowerCase() %>/Quotes]]
## Extras
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Misc",
  description as "Type"
	
WHERE campaign = <% tp.file.path(true).split("/")[1].toLowerCase() %>
  AND contains(type, "extra")

SORT file.name ASC
```
## The World
![[Worlds/???/Overview]]
![[Worlds/???/Locations]]
![[Worlds/???/Characters]]