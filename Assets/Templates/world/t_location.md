---
world: <% tp.file.path(true).split("/")[1].toLowerCase() %>
type: [location, <% tp.file.cursor(0) %>]
location:
---
## Overview

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE wold = <% tp.file.path(true).split("/")[1].toLowerCase() %>
  AND contains(type, "location")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE wold = <% tp.file.path(true).split("/")[1].toLowerCase() %>
  AND contains(type, "npc")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```