## Map
```leaflet
id: <% tp.user.get_parent(tp) %>_world
image: [[ <% tp.user.get_parent(tp) %>.png]]
height: 500px
lat: 50
long: 50
minZoom: 8
maxZoom: 10
defaultZoom: 5
unit: meters
scale: 1
marker: default, 39.983334, -82.983330, [[Note]]
```

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Factions"
	
WHERE wold = <% tp.file.path(true).split("/")[1].toLowerCase() %>
  AND contains(type, "faction")

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Regions"
	
WHERE wold = <% tp.file.path(true).split("/")[1].toLowerCase() %>
  AND contains(type, "regions")

SORT file.name ASC
```

```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Cities"
	
WHERE wold = <% tp.file.path(true).split("/")[1].toLowerCase() %>
  AND contains(type, "city")

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE wold = <% tp.file.path(true).split("/")[1].toLowerCase() %>
  AND contains(type, "npc")

SORT file.name ASC
```