---
world: <% tp.file.path(true).split("/")[1].toLowerCase() %>
type: [person, npc]
faction:
location:

Gender:
Level:
Class:
Race:
campaign:
- <% tp.file.cursor(0) %>: <% tp.file.cursor(1) %>
---
## Summary
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```