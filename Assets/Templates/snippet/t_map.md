```leaflet
id: <% tp.file.path(true).toLowerCase().replace(/[^a-z0-9_]/g, '_') %>
image: [[ <% tp.user.get_parent(tp) %>.png]]
height: 500px
lat: 50
long: 50
minZoom: 8
maxZoom: 10
defaultZoom: 5
unit: meters
scale: 1
marker: default, 39.983334, -82.983330, [[Note]]
```