function parent_folder(tp) {
	const folder = tp.file.folder(true).split('/');
	const parent = folder.slice(-2,1);
	return parent;
}
module.exports = parent_folder;