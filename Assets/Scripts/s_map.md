```js
const path = tp.file.getCurrentFile().parent().path();
const folders = path.split('/');
const folderNames = folders.filter(folder => folder.trim() !== '').map(folder => folder.toLowerCase().replace(/ /g, '_'));
const result = folderNames.join('_');
result;
```