## Campaign Overview
We begin our story at our 5 hero's third year at the [[Saurons School]]
Our heros first introduce one another. We introduce our characters, and decide what their relationships to one another may be starting this year. We then are assigned to hunting parties (the 5 heros) by [[Sauron]]. The party is then expected to select and complete their first task.

we have a magic d20 where we can, once per session roll extra numbers on an attack roll, or a check or something for a cinematic moment

### Party Quests
- [x] 🛫[[Session 09 - Entering Reinfeld]] Investigate the island ✅[[Session 10 - Reaching the Island ✅ 2023-08-12
- [x] 🛫 [[Session 10 - Reaching the Island]] Return to [[Saurons School]] after defeating the island ✅[[Session 13 - The Rescue Begins]] ✅ 2023-08-12
- [x] 🛫 [[Session 11 - Aftermath]] Find [[Venri Bronzeshard]] and make sure she is not dead. We suspect she may be after [[Eska and Venri Drunk Text]] ✅ 2023-08-12
- [ ] 🛫 [[Session 11 - Aftermath]] Each member of the party must purchase a goblet of the most expensive wine from the [[Maiden Manor]] to make up for the damage we caused. This will require 1g each.
- [x] 🛫 [[Session 11 - Aftermath]] Buy a drink ([[Eska]] promised 2) to each member of the crew aboard the airship when we next arrive in [[Reinfeld]] ✅ [[Session 14 - Arriving to Rakkesh]] ✅ 2023-08-12
- [ ] [[Session 14 - Arriving to Rakkesh]] [[Eska]] sent [[Wanda]] 10g to pay for the drinks we owe. they will wait a few weeks for us, and then pour one out for us if we don't make it back. Otherwise, they will have the drinks with us.
- [x] 🛫 [[Session 11 - Aftermath]] Eska is in possesion of a letter signed by [[Diana]] that says the group has defeated the [[DragonTurtle]]. He should present this to [[Sauron]] upon ariving at [[Saurons School]] to receive the bounty ✅[[Session 13 - The Rescue Begins]] ✅ 2023-08-12
### Unanswered Questions
- What is the black slime found around the [[Cimera]] 
- Why are creatures being corrupted with [[Mesamarine Crystal]]?
- Ask if [[Yyselin]]'s sister will attack or whatever if she is released
- figure out about [[Quiver]]'s old clan, and why she can't remember anything
### Personal Quests
- find out about [[Quiver]]'s mask
- find out about [[Sui-sui]]
- find out what's going on with [[Vaal]]'s fucked up neck
- Did [[Yyselin]]'s sister destroy her village? 
- find out what [[Quiver]] use to do as a raider, and see if their paths ever crossed.
- Only [[Yyselin]] knows about [[Eska]]'s past. He hasn't told anyone else
## Links
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Extras",
  description as "Type"
	
WHERE campaign = "nori_maidens"
  AND contains(type, "extra")

SORT file.name ASC
```
### Campaign
[[Campaigns/Nori Maidens/Sessions/Overview]]
[[Campaigns/Nori Maidens/Quotes]]
### World
[[Worlds/Terignyphtos/Overview]]
[[Campaigns/Nori Maidens/Characters]]