---
type: extra
description: prayer
campaign: nori_maidens
---
## Eska's original memory of the event
are mother moon has hid again
as her ignorant children wax and wane
a safer night will save the day 
a future night will carve

*elder lifts dagger*

hear our cries as the caller tries
to greet your return so we might earn
your blessing so we won't starve

*dagger into heart of mooncaller*

```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```