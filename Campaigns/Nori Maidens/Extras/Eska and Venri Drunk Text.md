---
type: extra
description: conversation
campaign: nori_maidens
---
Eska - Hello, I hope you remember me. A few years ago, I helped you sneak out of a city and to a sanctuary.  I believe I've "*cuts off*" met your eldest daughter, the water witch

Venri - i remember, you believe you what? that sanctuary was compromised. brought back into the city. there is a mean Tiefling lady. have mercy. please help

Eska - [[Sarphi]]? We can help. I know your daughter.

Venri - wait. my daughter? how do you know its my daughter?

Eska - Can't talk much more, strains my body. I work with her: the water witch. Can we help?

Venri - i think ive heard that name. do you mean my raindrop? please say yes. please hel-  the sentence is cut short

-- A few hours pass --
Eska - Are you alive?
Venri - ...

```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```