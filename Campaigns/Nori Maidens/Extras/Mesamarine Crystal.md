---
type: extra
description: object
campaign: nori_maidens
---
## Overview
Some monsters have their body converting into a crystal like material, and within this material, a tiny shard of mesamarine crystal can be found. This is the same material that is in the ring around Terignyphtos.

## First Contact
we find the crystals are slightly magical, but it doesn't appear to be a disease. On the ape's back, there is a small impact wound that seems to have healed over. There is scar tissue that is green. It is very tough, because of the crystal. There is a tiny shard of mesamarine crystal behind the scar tissue. This is the same matieral that is in the ring around the planet. It is a *highly* magical material. 
Some of it's bones had also been turned into this crystal. the mesamarine is highly conductive of magic.

There is a possibility that it either fell from space, and embedded itself into the creature, but it is more likely that this was planted in the creature. 

```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```