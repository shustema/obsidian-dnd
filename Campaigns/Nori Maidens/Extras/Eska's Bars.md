---
type: extra
description: prayer
campaign: nori_maidens
---
our mother moon now shields her eyes
as ignorant children wane and die
a safer night to save this day
a future night you starve

hear our cries oh elder, learn
to greet the moon in her return
tonight for safety we will earn
her blessing whilst you burn

```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```