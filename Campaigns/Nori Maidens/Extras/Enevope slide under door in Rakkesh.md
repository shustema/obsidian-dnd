---
type: extra
description: object
campaign: nori_maidens
---
I have sent 2 members of my team to assist in your endevours tonight. They will meet you at your destination.

```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```