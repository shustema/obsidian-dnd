---
campaign: nori-maidens
type: extra
description: 
---

```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```
Journal containing notes about what had happened to [[Yyselins Father]] and [[Worlds/Terignyphtos/Creatures/Characters/npc/Velmija|Velmija]]. [[Yyselin]] has only recently been able to open this. 