---
campaign: terignyphtos
type: extra
description: item
---

```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```
An amulet that [[Yyselin]] wears everywhere that contains both [[Worlds/Terignyphtos/Creatures/Characters/npc/Velmija|Velmija]] and [[Yyselins Father]]. They seemed to be imprisoned in this locket. 