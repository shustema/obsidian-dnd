---
campaign: nori-maidens
---
#nori-maidens 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## Into Reinfeld
### Finding the Docks
[[Wistaria]] goes into a tavern called the dirty beard 

[[Sui-sui]] is looking for a butchery

[[Yyselin]] finds the maiden manor, and finds a small bakery in the back of it. She finds Diana talking to a woman at the bar. The other woman has grey hair and pale blue skin.

[[Eska]] and [[Quiver]] find a map of [[Reinfeld]]

[[Quiver]] says his parents don't hug him

we find [[Diana]] and she offers us the option to either travel by air or by sea
[[Diana]] signed the note that [[Sauron]] gave us

[[Yyselin]] used to like to fly but can't anymore

we hire a crew for 4g and 2g per day additional, and have 2 hours to kill before the crew is ready so we go to a resturant. This is called the devil's diner which is run by [[Ramson Gourdy]]

### Passing Time
[[Yyselin]] runs off to go to the air docks, and meets up with 2 people, one very thin, and one very bulky. She is looking for [[Wanda]], because she has questions. [[Wanda]] is the larger of the 2. 
[[Yyselin]] is asking about how much it would cost for her to go to the same place her father sent her before.

they were out LOOKING FOR SOMEONE IN THE DESERT. "there are some sketchy people out in the desert" [[Session 08 - Solo Session#Found a woman]]

they agree to take her so long as they can find other people who want to go to [[Rakesh]] 
They then go to a clothing store.

I see a teifling that looks sussie wussie by the dirty beard
We go into a magic 