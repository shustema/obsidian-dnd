---
banner: "![[Eska.jpg]]"
banner_y: 0.168
campaign: nori-maidens
---
#nori-maidens 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## Dream Sequence
### I have a dream
I have a reoccuring dream
I'm in a mooncalling ceremony. There are a bunch of people around me wearing robes that denote their ranksin the [[O'Deorain]]. Everyone has the masks, the enlightened wear burgendy robes that have a gold trim to them. The elders wear white and gold(trim) robes, and the mooncaller wears black robes. my robes are burgendy. 

we are standing around a painted sigil on the floor. An enlightened enters the circle along with the mooncaller. The mooncaller lies in the center of the circle, and the enlightened leaves to join the circle. 

The elders gather around the mooncaller and do a chant: 
![[Mooncaller's Chant#Eska's original memory of the event]]

a sliver of the moon appears after. 

[[Eska]] believes this ritual brings the moon back. they do this every new moon.
The mooncaller is not one of our members, but from where I don't know

### Getting a Mooncalller
3 members, one of which is [[Eska]], and an enlightened head out to find a new Mooncaller. 
They are going to [[Rakkesh]] 
My job is to keep the mooncaller and enlightened safe. I would rather die then see these 2 come to harm.

[[Eska]] is honnored to be called to do this

I am waiting outside a building while the enlightened goles into the building to get the mooncaller. 

A burley earth genasi is comming up to us, yells at us, and pulls a knift

I remember this dream wrong, but I remember knocking this guy out.

### The Final Ceremony
same actions as always, but this time everything feels different.
The mooncaller approaches very calmly, and lays down seemingly willingly, and she seems very neutral about everything.

as they bring out the mooncaller, I have a little doubt because of what happened in [[Rakkesh]]. The mooncaller seems out of it, and this seems weird. 
The chant seems weird to me this time around

as she is stabbed, she is no longer neutral. The "mooncaller call" is a scream of pain and fear as she seems to be snapped out of it.

As the moon should appear, one elder licks the blade, and the other casts some spell.

in a nightmare I see their glowing red eyes as they lick the blade, and they look up at me
## Rakkesh 
### Minatour
sold some fake tuska to a earth genasi,
pissed off a minatour

### Found a woman
I save a woman who looks elven with dark hair and a green cloak
she needs  help getting to an elven hideout outside of the city

I take her to [[Ozo]] who may know where to go
rock with a carving of a triangle with a line through it denotes the entrance of the cave

we head west, and there is a small mountain range. We begin looking for the cave.

the girl's name is [[Venri Bronzeshard]] (random name)
cave has a small opening, and the girl turns into a wolf to go in
### Meeting Bolin
me and [[Bolin]] had a one man band style scam off, and then partnered up

### Sarphi
I have been living in hotel rooms, and working with [[Sarphi]] a little while I've been in [[Rakkesh]] 
I come to her and ask for a job, and she says it will be harder
Have to go kick these people out of our territory

it goes bad
[[Bolin]] fights with him 
tells him "you elves are all the same"

