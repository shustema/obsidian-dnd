---
campaign: nori-maidens
banner: "![[eska-ai.png]]"
banner_y: 0.28
---
#nori-maidens 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## Journey to Rakkesh
[[Eska]] talks to [[Wanda]] to ask if she can stop over the [[Saurons School]]. We try to figure out a way to get down, as the ship circles the [[Saurons School]]. [[Yyselin]] jumps over the edge, and turns into an eagle. [[Eska]] and [[Gila]] both fly down after her not realizing the eagle is [[Yyselin]], but we figure this out.

[[Yyselin]] and [[Quiver]] talk. she tells [[Quiver]] she was right that she is being held back by herself, and asks for her help training. [[Quiver]] agrees.

[[Quiver]] makes a bag out of the [[Kraken]] pelt. This bag is like a belt.
[[Eska]] will gather the group and warn them about [[Sarphi]]

### Yyselin Confronts Eska
[[Eska]] and [[Yyselin]] talk about how [[Eska]] met [[Venri Bronzeshard]], and a little bit about his past. [[Eska]] tells her about [[Bolin]] and gives a few more notes about [[Sarphi]]. [[Eska]] also mentions that she is right to be mad at him, just not over her mother.

### Fake Rituals
[[Sui-sui]] captures a goose as a sacrifice so [[Eska]] can ask the Moon if her egg is fertilized
Just a whole bunch of shit happens afterwards.

[[Eska]] will perform a ritual to ask if the egg contains a life. [[Eska]] does this in a very needlessly flashy way.

### Vineyard
[[Eska]] and [[Yyselin]] will make a swanky new armor piece for [[Eska]] with the remaining vines
The party learns that the bag is a bag of holding

## Pit Stop at the School
### Asking Sauron
[[Yyselin]] and [[Gila]] go down to get the bounties.
[[Yyselin]] asked [[Sauron]] if he knew the truth about her father and the [[Yyselin's Journal]]/[[Yyselin's Amulet]].
He says her dad called him to get her and bring her to the school.

She presses further to see if he knows the truth about the items. She says she now knows the truth, and she can't do this on her own. 

He responds, that she has drastically misunderstood the purpose of her coming here. He says her dad and him had discussed she was to come here and to learn how to deal with dangerous creatures.

He says this was never meant to be a solo mission, and he had put her on a team of capable people. 

She responds, I don't even know where to begin with this (holding up the [[Yyselin's Amulet]]) and that she's terrified about her friends learning the truth because it means she would have to tell them everything.

He confronts her and basically says she can trust her team, and says she seems to be on the right path anyways.

[[Eska]] sends a message to [[Pyria Keyfeld]]
"Raindrop is coming to help, and so is father and little flame ::thumbs up::"
[[Pyria Keyfeld]] responds
":smile: :teeth smile: :crying:"

[[Eska]] messages [[Bolin]]
"I hope you're doing well"

[[Yyselin]] removes her ring while talking to [[Eska]] and shows the party what is on her arms. She says she doesn't want to hide anymore.

[[Eska]] tells his backstory to [[Yyselin]]. She doesn't hate him after finding out how much of a shitheel he is. 