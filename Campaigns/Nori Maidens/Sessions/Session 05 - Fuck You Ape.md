---
banner: "![[sauronsschool.png]]"
banner_y: 1
campaign: nori-maidens
---
#nori-maidens 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## Walking back from the [[Froghemouth]]
On the trip back, we find a giant ape that attacks us
the ape looked fucked up, it has green crystals all over it, and it has patchy fur. Where the exposed fur is, it has been replaced with green crystals. With some difficulty, we take it down. 

upon bringing the ape back to the school, we discover a [[Mesamarine Crystal]] embedded into it's body, causing his flesh and bone to convert into crystal.

There is a possibility that it either fell from space, and embedded itself into the creature, but it is more likely that this was planted in the creature. 

we talk about [[Yyselin]]'s findings, and postulate on what could be happening

## Back at the School
The teachers call a meeting and say there is a magic item shop

[[Vaal]] and [[Eska]] are idiots and fucked, and sold their souls to [[Borious the Glorious]] for like 15 - 50g