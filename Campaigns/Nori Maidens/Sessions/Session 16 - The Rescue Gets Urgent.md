---
campaign nori-maidens
banner: "![[eska-faceless.png]]"
banner_y: 0.32
---
#nori-maidens 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```

## The Meeting Continues
The shipment is being picked up very soon.

The one time [[Eska]] went to collect a mooncaller, he met at a building. [[Eska]] proposes a plan to make counterfiet robes, and attempt to pick up the the mooncaller. Later, [[Quiver]] asks eska more about what the [[O'Deorain]] do and what type of magic they have, [[Eska]] basically says he doesn't know. 

### Night Watch
- [[Yyselin]] is fiddling with her necklace on her watch shift.  
- [[Quiver]] is talking with [[Gila]] in draconic on her watch. [[Yyselin]] overhears this conversation.
"The spirit of your mother's egg. you and I both have lost our mothers and are both powerless. We can't change this, but hopefully we can change this for our friend." [[Quiver]] remonices on how she doesn't remember anything about the violent acts she did in her tribe, and why killing that doesn't mean anything to her. 
- [[Eska]] and [[Homil]] argue during their watch. After [[Yyselin]] eavesdrop, [[Homil]] warns her that he will be keeping a close eye on the party.

## Counterfeiting
[[Yyselin]] and [[Eska]] walk up to [[Oraa]]'s pottery tent.  [[Yyselin]] asks her to make a counterfeit of [[Eska]]'s mask. [[Yyselin]] rolls a nat 20, and persuades him to help.

[[Sui-sui]], [[Wistaria]], and [[Quiver]] go to [[Rascaal]]'s tailor store, to tray to get his help as well.
they offer him 80g, to do the cost, and keep it quiet. 

### The Results
[[Rascaal]] finishes his work and it is passable. 

## The Heist
we break off into parties. 
"you guys have come to pick up another one?"
[[Yyselin]] warns the group that they have already come to get one.
[[Yyselin]] enters the building. It is dark with no windows. 
[[Yyselin]] tells them, the first shipment was intercepted, and the first elder sent them to remedy this situation and investigate.
[[Yyselin]] is found out.

[[Yyselin]] turns into an elemental on him for saying 
"you two smell the same"

we all take turns trying to talk her down after she kills this orc
[[Yyselin]] says "i've become like her ([[Worlds/Terignyphtos/Creatures/Characters/npc/Velmija|Velmija]]) and I was supposed to help her, not be like her" 
she looses her mind, and we fight her.