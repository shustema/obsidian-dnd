---
banner: "![[deepcrow.png]]"
banner_y: 0.212
campaign: nori-maidens
---
#nori-maidens 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## Downtime Shenanigans
[[Eska]] buys 2 donkeys, and a cart
[[Sauron]] agreed to put a build order in for a stables
## Quick Adventure
The gang goes on to fight [[Deep Crow]] in the plains
we beat the absolute shit out of it

[[Eska]] is taking an eye
[[Vaal]] is taking a foot
[[Yyselin]] is taking the mandable
[[Wistaria]] is taking some feathers

"horses" name are mark and eucaliptus
[[Eska]] materials for 2 padded bardings