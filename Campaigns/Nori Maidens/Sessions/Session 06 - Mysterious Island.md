---
banner: "![[suisui.png]]"
banner_y: 0.176
campaign: nori-maidens
---
#nori-maidens 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## New Student
[[Sui-sui]] joins the party. We learn that her previous group has all been killed, and she is the only survivor. She also eats her own egs..... 

## Mysterious Island
We ask [[Sauron]] for money for a ship. 
He says to find diana in the maiden manners in [[Reinfeld]].

[[Eska]], [[Quiver]], and [[Wistaria]] forage in the woods
[[Wistaria]] gets stuff for paper, [[Eska]] and [[Quiver]] get wood and stones, and make shitty totems

[[Eska]] and [[Yyselin]] talk about her sister, and talk about scars
[[Yyselin]]'s sister is a clergyman, (maybe to make up for her dead parents)