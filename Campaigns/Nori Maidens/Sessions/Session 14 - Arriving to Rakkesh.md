---
campaign: nori-maidens
---
#nori-maidens 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## Continuing the Airship Trip
[[Eska]] gives [[Vaal]] 2 bars of soap, and asks how he's doing after the fight with the [[DragonTurtle]]. [[Vaal]] talks about how he kind of blacked out and doesn't remember what happened.

[[Yyselin]] and [[Vaal]] talk about the [[Yyselin's Amulet]] and [[Yyselin's Journal]], and she tells him why they are going to [[Rakkesh]]. [[Yyselin]] has experienced a power similar to the [[Mesamarine Crystal]]s before.

## We Arrive
We first make our way over to the cave where [[Eska]] first escorted [[Venri Bronzeshard]] to in [[Session 08 - Solo Session]]. We go to the cave, and [[Sui-sui]] turns into a cloud of butterflies to go scout ahead to look at what is in the cave.

Most of the party makes it's way into the cave, except for [[Eska]] who stays outside to stand watch. We find their farm, and [[Eska]] sends a message to [[Pyria Keyfeld]]
"We are at the farm, what's your location :thums: :shrug:"
"Oh no, tell me you're joking, leave now"

We get jumped by a bunch of undead and kill them, clearing out the cave
[[Yyselin]] goes into a rage during the fight upon hearing that her mom isn't here.

[[Eska]] sends another message to [[Pyria Keyfeld]]
"farm is safe now, find us there"
"cant"