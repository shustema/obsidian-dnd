---
campaign: nori-maidens
---
#nori-maidens 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```

## Out of Danger
[[Bolin]] is wearing red white and gold robe, and walks away after smiling and winking at [[Eska]].

We go back to the tavern to rest for an hour. [[Eska]] opens the door to come face to face with [[Bolin]]. [[Bolin]] tells [[Eska]] that [[Sarphi]] has been displaying incredible magic abilities recently. She is stronger.  [[Bolin]] offers to aid the party.

[[Eska]] and [[Quiver]] argue about whether or not the civilians should fight. [[Quiver]] talks about how [[Yyselin]] is a liability and she overhears. She kind of pouts, then leaves the room. the party then goes to explore the city

### Exploring the City
[[Quiver]] buys a truth  potion
[[Eska]] buys a cloak of billowing.
[[Yyselin]] wanders the city in a daze, thinking about what [[Quiver]] said to her.

we go back to [[Oraa]] to try to find the captured prisoners to get some more information.
[[Yyselin]] and [[Quiver]] split up and search, and [[Eska]] [[Wistaria]], and [[Sui-sui]] look in a different group.

[[Eska]] talks with [[Jasper]] who we learn is part of the underground railroad. He leaves [[Eska]] a letter [[Enevope slide under door in Rakkesh]] in the morning. [[Quiver]] summons a pack of dogs and we go to track [[Pyria Keyfeld]] down. 

We fight some mimic dogs, and when the last one dies, it says Raindrop.