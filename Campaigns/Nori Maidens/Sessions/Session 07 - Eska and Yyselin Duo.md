---
banner: "![[Eska.jpg]]"
banner_y: 0.176
campaign: nori-maidens
---

#nori-maidens 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## The Search Begins
[[Eska]] and [[Yyselin]] set of to look for a giant elk 

[[Eska]] asks [[Yyselin]] about the ocean, and she looks longingly into the distance thinking of better times

[[Yyselin]] gets eaten by a vine monster, and she uses a wind tunnel explosion to get out. 
while doing this, she has water like waves on her eyes (like anime), and pops of electricity comming off of her horns

[[Yyselin]] gets her power from the elementals kind of? from typhus probably
[[Yyselin]] is only alive because of her air/water powers and her sister's fire powers granted by the elementals. 
## Going Home
we find a parasidic gas spore on the side of a tree. These are INCREDIBLY poisonous.

[[Eska]] almost died, he got poisoned
##  A New Business Venture
we have chestplate, 
we need back, and both shoulders
[[Yyselin]]'s mother's name was paria keithild