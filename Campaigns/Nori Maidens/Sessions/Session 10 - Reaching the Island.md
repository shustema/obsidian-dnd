---
banner: "![[dragonturtle.png]]"
banner_y: 0.332
campaign: nori-maidens
---

#nori-maidens 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## Quick Detour
### More Shopping
[[Wistaria]] [[Yyselin]] and [[Vaal]] go to [[Athena]]'s jewlery store. [[Wistaria]] is looking for an amathyst like the one she already has. This shop has no magic items, so [[Wistaria]] instead buys a circlet with 3 amathysts embroidered into it.

[[Yyselin]] is looking for earings, that are orange or yellow. She ends up buying yellow flower earings.

We get to hear the best character [[Rilby Wimple]] again. 
## The Hunt Begins
### Reaching the Island
while we are traveling [[Sui-sui]] is sunbathing, [[Vaal]] is looking at the craftsmanship of the boat
We see the island. [[Eska]] is getting sick over the edge, [[Yyselin]] is a sneaky bastard and saw [[Eska]]'s face while he was throwing up.

upon arival, we see a small island (smaller than the boat), [[Yyselin]] drags [[Eska]] into the water because she hates him, and is secretly trying to kill him. [[Eska]] gets back onto the boat, jumps on a tensor's floating disk like an idiot, realizes that does nothing, and gets back on the boat. 

There is a growth under the water. These growths have mouths. The captain yells battle stations, and gears start moving in the ship.
The monster is a [[DragonTurtle]]. The ship spits out a melee platform, and probably shoots it with a harpoon, but we don't see it. 

the thing fucks us up, but we do eventually kill it.

[[Vaal]] and [[Yyselin]] are both visibly traumatized after the fight seemingly because the [[DragonTurtle]] burned everyone on the team with it's breath attack.
