---
campaign: nori-maidens
---
#nori-maidens 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## The Hunt Continues
We continue to follow the dogs. After some travel, we come upon 8 people about an hour ahead of us.  They all have long gray robes. 

[[Yyselin]] runs off away from the group to attack the people, but [[Homil]] dive bombs her so the party can catch up.

## The Fight Begins
we sneak in and start ripping off masks
When the elder attacks [[Quiver]], he makes a sound similar to the sound when [[Homil]] dies.

[[Bolin]]'s group and a loxodon join the fight

### Freeing [[Pyria Keyfeld]]
[[Eska]] and then [[Yyselin]] both try to break [[Pyria Keyfeld]] free from the charm. [[Yyselin]] is stopped by the white robed figure in her mind's eye.  lightning from [[Yyselin]]'s pendant attacks the guy, and she pushes him away and free's her mom. [[Pyria Keyfeld]] looks at [[Yyselin]] with a flood of emotions, speechless. 

She touches [[Yyselin]]'s face, and says raidrop. At this point, the white robed figure lets out a scream.

![[Eska's Bars]]

the white robe responds to [[Eska]].
"Oh, we were always like this, you were just too stupid to realize."

[[Eska]] fires a guiding bolt at him, and he turns to dust.