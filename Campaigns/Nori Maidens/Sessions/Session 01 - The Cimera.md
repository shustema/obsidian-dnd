---
banner: "![[WhiteVariantCimera.jpeg]]"
banner_y: 0.068
campaign: nori-maidens
---

#nori-maidens 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## Getting a Task
The party then goes to the task board to select task. The three options are: 
* go to the [[Desert]] and hunt (something)
* go to the forrest and hunt (something)
* go to the [[Plains]] and hunt a [[Cimera]] 

[[Eska]] asks not to go to the [[Desert]]. The party is split evenly between the [[Desert]] and forrest, so we grab the [[Cimera]] hunt in the [[Plains]] as a compromise.

The party sets out to fight the [[Cimera]].

## The Hunt
The party makes it to the [[Plains]] without issue. Upon arrival, we see a group of centaurs running around, but they are not close enough to care about us. They are the ones who contracted us to take care of this [[Cimera]]. 

We come to a clearing, and notice the grass in the area has been burned, but there are no tracks leading to it, and there is nothing in the epicenter of the burn that may have been attacked. 

While we are looking at the area, a [[Cimera#White Variant Cimera]] descends onto the party. we begin to fight it, and [[Wistaria]] notices that it is not a normal [[Cimera]]. We knock it unconsious, and investigate the area. The only thing we find is a rock covered in [[Black slime]], but unable to find anything else, and with no way to bring the body with us to the school, we kill it - much to [[Yyselin]]'s shagrin. 

The party members each claim a trophy from the [[Cimera]]
[[Eska]] takes an eye from the lion head. 
[[Yyselin]] takes ... 
[[Quiver]] takes ... 
[[Wistaria]] takes ...

The party leaves the cimera, and begins to return to the school. On the way, the party must sneak past a group of [[AxeBeak]]s. [[Yyselin]] was almost spotted by the beasts, but she wildshapped, and jumped onto [[Eska]], and the party was able to sneak past.

