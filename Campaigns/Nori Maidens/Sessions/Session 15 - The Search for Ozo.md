---
campaign: nori-maidens
---
#nori-maidens 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## Yyselin's Temper Tantrum
[[Eska]] calms [[Yyselin]], and tells her he has an idea of where we can find her mother. 

### To Rakkesh
We make our way back, and power through a sandstorm

The party buys 2 rooms outside of [[Sarphi]]'s territory
[[Wistaria]] goes to get some drinks in [[Sarphi]]'s territory, [[Sui-sui]] goes to look at the pets, and [[Eska]] and [[Yyselin]] hang back at the inn to take a short rest. 

[[Eska]] goes to the old laundry matt to say a quick prayer, and then met up with [[Ozo]] to try to figure out where [[Pyria Keyfeld]] is at. [[Ozo]] agrees to meet him at the inn at midnight.

[[Sui-sui]] gets drunk

[[Ozo]] says a group of guards found some people in the mountain. they were "taken in"
"she's" involved. at the end of the conversion, he points out the window and says "they don't have much time left" and points to a very tiny sliver of a moon