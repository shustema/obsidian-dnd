---
campaign: nori-maidens
---
#nori-maidens 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```

## After the Fight
[[Sui-sui]] pulls the arrows that she shot at [[Eska]] out of his back, and he cries in a very uncool way. 

[[Yyselin]] talks with [[Pyria Keyfeld]] for the first time in years. [[Yyselin]] casts a spell as her and [[Pyria Keyfeld]] stare at [[Yyselins Father]].  [[Pyria Keyfeld]] tells her that [[Yyselins Father]] was developing something like the locket in case something went wrong, and suggests that they go back home to get his papers and notes. 

[[Eska]] says hello to [[Venri Bronzeshard]] and she recognizes him.

[[Eska]] speeks with [[Ihlah]] and and she tells the party that [[Bolin]] is working on a plan to take down [[Sarphi]].

### Back to [[The Dune]]
[[Yyselin]] orders a ton of food for [[Pyria Keyfeld]] and they go up to a private room to talk. 
[[Yyselin]] asks why she wasn't around while they were growing up. 
[[Yyselin]] tells the story of how [[Worlds/Terignyphtos/Creatures/Characters/npc/Velmija|Velmija]] lost control of her powers, and tells her that she wasn't able to stop [[Worlds/Terignyphtos/Creatures/Characters/npc/Velmija|Velmija]] on her own, and passed out as [[Yyselins Father]] showed up. she tells her mom that before they can go home, they have to destroy [[Sarphi]] and the [[O'Deorain]].  

[[Yyselin]] also tells [[Pyria Keyfeld]] that she is loosing control of her powers (elemental form) and that maybe [[Pyria Keyfeld]] would be able to help her learn to control them. her mom agrees to help her to train and learn how to control her powers. 

[[Quiver]] throws sand all over her room to have an excuse to spend the night in [[Pyria Keyfeld]]'s room.
[[Quiver]] then gets beat up after breaking into a minitours house
[[Eska]] goes and leaves a note at [[Oraa]]'s tent, asking to meet in the morning at 9 at [[The Dune]]

[[Quiver]] tries to sleep in [[Pyria Keyfeld]] and [[Yyselin]]'s room, but [[Yyselin]] kicks him out.

### The next morning
[[Oraa]] meets up with [[Eska]] and [[Eska]] asks him to have any supporters in the town put clay pots in thier windows with the handprint on them. He agrees to this. 

We come up with a plan to get more information. Infultrate a fight club.

"do you have any friends in the den, or just around? looking for trees"
"yes, one waiter, I think the nearest tree is 3 miles south. I think it's acacia"