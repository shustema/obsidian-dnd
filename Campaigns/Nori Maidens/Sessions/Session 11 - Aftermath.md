---
banner: "![[Eska.jpg]]"
banner_y: 0.176
campaign: nori-maidens
---
#nori-maidens 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## Off The Boat
The [[Maiden Manor]] was built by steve from minecraft. This is cannon.

[[Eska]] sends a message to [[Sauron]]
"We killed the island, we couldn't get a trophy because it sank to the bottom of the ocean, lmao DX. Are there any further instructions. Click here to respond."

[[Sauron]] responds
That is both good news, and unfortunate news. Come back to school for a new bounty.

## The Next Day
[[Eska]] goes and finds soaps, mint and citrus and a scrub brush. 

[[Quiver]] goes to a potions store. Things that people want are expensive, so quiver can't buy a fire resistance potion.

### Save the Trauma for your Mamma
[[Yyselin]] is breaking down in her room, and pulls out an item "Dad, I don't know what to do anymore, I can't even protect my friends" She is looking at the [[Yyselin's Journal]]. She is asking her dad to help her, and that she doesn't know what to do, and that "she" would be revealed to her.
[[Yyselin]] feels a finger on her forhead, and the [[Yyselin's Journal]], that looks to have a closed eyelid, is no longer closed. The journal now opens. 2 things fall out, and envelope, and a bookmark falls out revealing itself to be a pendant, with a centerpiece that is a smooth circular stone.

The envelope is addressed to [[Yyselin]], her sister, and her dad. 
[[Yyselin]] cries when reading the letter "Mother...."
The pendant is her dad's necklace that he wore every single day until the last day before he left. [[Yyselin]] has a third eye on her forhead while she is attempting to open the pendant. inside is a picture of her and her sister, and on the other is her parents.

"if you only hold on a little longer, I'll come and find you" says [[Yyselin]] after seeing this picture.

her back from the neck down is covered in burns.
[[Yyselin]] talks to [[Eska]] about a venri who is in the hideout in [[Rakkesh]].

[[Quiver]] tries to set the bed on fire to prove a point.... and [[Yyselin]] stomps him into the ground.

[[Vaal]] wakes up and askes [[Eska]] "Where are we, and why haven't we gotten on the boat yet?"
[[Quiver]] says "Their broken, their useless"
The damage will be taken out of our pay for the job.... great
[[Sui-sui]] says to [[Quiver]] "if you ever do anything to endanger the egg again, I will eat you" [[Quiver]] gets a chill down her spine

we get kicked out of the [[Maiden Manor]], and are told not to return
Vaal has scars all over the left side of his body

[[Eska]] goes to the bar to drink. [[Vaal]] and [[Yyselin]] go off on their own and talk. she tells him she doesn't want to hide any more, and [[Vaal]] tells her, "this isn't happening again". He is pissed

As [[Yyselin]] reads the [[Yyselin's Journal]], it describes the final day in her hometown, but not the way she remembers it. She says we need to get home, and I need to apologize to [[Quiver]]. She shows [[Vaal]] the burn marks on her arms. [[Vaal]] responds "[[Quiver]] isn't the one that should receive an apology."

### Drunk Texting
[[Eska]] will drunk text [[Venri Bronzeshard]]

![[Eska and Venri Drunk Text]]

After Eska relays the message from Venri, [[Yyselin]] gathers all of us to go to the airship. [[Yyselin]] says raidrop is the nickname her father gave to her. 
[[Eska]] goes to buy a new mask and cloak for a disguise.

### The airship
we set off on the airship to go to [[Rakkesh]] with [[Wanda]]

[[Eska]] sends a final message to [[Venri Bronzeshard]]:
Are you still alive?
he gets no response