
---
type: extra
description: object
campaign: excurian-explorers
---
- When opened released [[The Figure]]


```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```