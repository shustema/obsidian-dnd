---
type: extra
description: description
campaign: excurian-explorers
---

"The Gloming Pact is an oportunity. it represents a way forward. and end to the stalemate. and to [[The Figure]], it means an end to hunger"

```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```