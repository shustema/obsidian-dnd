---
campaign: excurian-explorers
type: extra
description: description
---

```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```
## Crew
Captain: 1g/day
First Mate: 1g/day
Navigator: 1g/day
Quartermaster: 1g/day
Cook: 1g/day
Carpenter: 1g/day
Security: 1g/day
Medic: 1g/day

10 crew: 3g/day
30 crewmembers: 9g/day

### Our Paid Crew
cook, security, medic, mascot
10 crew
7g/day
## Money Making
- Each ton of cargo (13 currently) 100g per ton (basic cargo)
1300g for full load