---
type: extra
description: note
campaign: excurian-explorers
---
Where possible keep the item away from common areas
The key is to be stored within 15 feet of the item and is to be directly observed at all times
Keep an armed guard, rotated MINIMUM every two hours
You do not recognise her
Do not look at the item for longer than three minutes at a time
Do not, under any circumstances, fall asleep within 15 feet of the item
You do not recognise her
Any hallucinations must be verified with a third party and noted
YOU DO NOT RECOGNISE HER
Lolth guide you

```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```