---
type: extra
description: note
campaign: excurian-explorers
---
"The [[Swallowtails]], six tales, keep this notes, and await furthur instructions"

```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```