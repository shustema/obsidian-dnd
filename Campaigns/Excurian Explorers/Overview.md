
---
banner: "![[Excuria.png]]"
banner_y: 0
---
## Campaign Overview

### Party Quests
- [/] 🛫 [[Session 01 - A New Beginning]] Retreive a stone coffin. 1000g each if we do so, 500g bonus if there are no witnesses.
- [x] 🛫 [[Session 02 - Meeting Tavir]] If you hear a cannon, something's gone wrong, so return to the ship ✅ 2023-05-13
- [ ] 🛫 [[Session 01 - A New Beginning]] find The [[Razors Edge]], and try to find where [[Captain Embross]] went after leaving their. when he dissapeared
### Unanswered Questions
- who is [[Captain Embross]]?
- What did [[Selkie]] "try" and why did she need to tell the person in the [[Razors Edge]]
- What is the Shadow Dancer Ubral? They are the one that did the hit on [[Oocelazok's Wife]] I think.
- What are the spirals that keep appearing like in [[Session 08 - Downtime]]

### Personal Quests
- do we even want to bring it back to that ship? the crew is going to try to open it for sure
- Read the book that has the captains notes. It is in dwarvish, so we need to get it translated. Maybe [[Grimbol]] can translate this for us.
- Keep a close eye on [[Ridley]], so he doesn't attack the crew. We should stay with this coffin on the ship to ensure the crew doesn't lose it.
- The flouriana is captain tavir's ship, the silent sisters is the direlect ship we got the coffin from.

## Links
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Misc",
  description as "Type"
	
WHERE campaign = "excurian-explorers"
  AND contains(type, "extra")

SORT file.name ASC
```
### Campaign
[[Campaigns/Excurian Explorers/Sessions/Overview]]
[[Campaigns/Excurian Explorers/Quotes]]
### World
[[Worlds/Excuria/Overview]]
[[Campaigns/Excurian Explorers/Characters]]