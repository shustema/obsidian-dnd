## Player Characters
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Players",
  location as "Location"
	
WHERE world = "excuria"
  AND contains(type, "plc")

SORT file.name ASC
```
## NPC Characters
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's",
  campaign.excurian-explorers as "Afinity",
  location as "Location"
	
WHERE world = "excuria"
  AND contains(type, "npc")
  
SORT file.name ASC
```
