---
campaign excurian-explorers
---
#excurian-explorers 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	type as "Type"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## In the beginning

we start in the underdark, there is no sky, or stars. We are seeing tiny distant pinpricks of light that are the glowing rocks that look like a stary night. 

we pan down to see a city. [[Tarnhold]] is where we are staring.
It is in [[Deck Market]] where we begin our journey. 

we have each been handed a note which is a job. This note is handwritten, and we are low on cash. It reads: 
![[Job Note 0]]

text dissapears on the note, knock twice, ask for [[Gaieuss]]

to follow the note, we go to the [[Swallowtails]], knock twice, and ask for [[Gaieuss]].
we meet her, and she asks us to sign a magically binding contract, which we do.

we then go to get a drink at the loo ship?
we are on the lookout for [[Captain Tavir]]