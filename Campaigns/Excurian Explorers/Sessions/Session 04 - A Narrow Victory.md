---
campaign excurian-explorers
---
#excurian-explorers 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## For next session
figure out [[Beast]]'s short term and long term goals
figure out what you will do in your downtime at [[Tarnhold]] after this mini campaign arc wraps up.

## The Battle Continues
[[Selkie]] summons a hawk named "Tony". Tony the Hawk flies down the corridor and attacks the enemy.

we hear a cannon fire off in the distance while we are fighting. We need to get back to the ship. 

[[Oocelazok]] goes down, and is making death saving throws. [[Selkie]] is close to falling as well. 

The mage we are attacking is from [[House Ralek]].

We hear another cannon shot, but this time from a different position

as [[Oocelazok]] is making death saving throws, he hears the voice of [[Oocelazok's Wife]]: "come home dear"

## After the battle
[[Ridley]] comes out of hiding, and [[Beast]] attacks him briefly, until  [[Oocelazok]] calms him. [[Oocelazok]] convinces him to come back to the other ship as a crewmember.

[[Beast]] and [[Spark]] carry the [[Stone Coffin]] up. We hear cannon fire from 2 separate locations from another place.

as we look out from the top deck, we can vaguely see people running around on the deck of the ship. we hear a cannon shot from somewhere out to the east, but we don't see the ship. It looks as though the ship has gone fully blind.

[[Beast]] confronts [[Ridley]]. 
he says that they all saw some things, and that they would just write it down as they saw them. the captain wouldn't do anything about it, and just told them to get back to work. When you are at sea, some people just break. sometimes we are all seing the same stuff, sometimes we are all seing different stuff. We were all on edge.
Gesepy was always looking for a chance to be a hero, he was always drawing at shadows, and jumping at things that werent there.
When [[Ridley]] was up in his bunk, he couldn't sleep, because he kept thinking he heard things from down below, even though he knew it wasn't real. Everyone else was asleep, and he heard fighting. 
[[Ridley]] tried to wake the others up, but couldn't, they were mumbling things. 
[[Ridley]] runs downstairs to get some weapons, and [[Gesepy]] is there. [[Gesepy]] was going to open the [[Stone Coffin]], and had started turning the key, so [[Ridley]] had to kill him. 

Everytime [[Selkie]] hears a shot from accross the water, she flinches a little. 

we pack up the [[Stone Coffin]], and leave The [[The Wwwwhimsy]] to return to the [[Flouriana]]

### Return to the Ship
We see [[Grimbol]] looking over the edge of the [[Flouriana]] waiting for us. He tells [[Captain Tavir]] that we have returned.  They winch the boat up, and we get back to the ship.

[[Oocelazok]] heals a guy, the ship goes full dark, and we end the session.