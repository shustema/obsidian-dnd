---
campaign excurian-explorers
---
#excurian-explorers 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## Boarding the ship 
as we board The [[The Wwwwhimsy]], we see bodies. They had their weapons drawn when they died
the maybe captain was attacked in his sleep

This is a [[House Ralek]] ship.

we find a set of instructions in a drawer with a fake bottom left behind by the now deceased crew. the note reads: 
![[Stone [[Stone Coffin]] Shipping Instructions]]

we look around thie ship, more dead, and we find the cofifn in the basement with the key in the lock
oz takes the key out and we find a crewmember alive in the basement named [[Ridley]]. He tells us that one of the other crew members began to open the [[Stone Coffin]], but was killed before he could fully turn the key, and so was everyone else aboard the ship. We got no more, information for the time being.

we hear dunking on the side of the ship. it is the sound of ropes, and people climbing up the ship.

Something about this situation doesn't quite add up to selkie, but we are not sure what yet. 
### Things that don't add up
- why was [[Ridley]] upstairs, and why did he run past the [[Stone Coffin]] when hiding from us
- why did that guy open the [[Stone Coffin]]
- why did the guy stop unlocking the [[Stone Coffin]]
- [[Ridley]] is lying

## A Battle
The captain says "The test run seems to have gone perfectly" as he first arrives in the ship.