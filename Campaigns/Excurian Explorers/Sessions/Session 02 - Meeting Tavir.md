---
campaign excurian-explorers
---
#excurian-explorers 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## The Tavern
[[Beast]] remembers a group called the battle bards which played a hurdy gurdy from another life. It is a sturdy hurdy gurdy.

## Tavir's Ship
after getting kicked out of the tavern, we go to [[Captain Tavir]]'s ship, the [[Flouriana]]. 
The deck of his ship seems to be mostly occupied by flower beds and vegetable patches. hanging above each bed, is a lantern that is glowing with a bluish light

we bunk in his ship.
[[Otto]] comes up to talk to [[Captain Tavir]]. 
we are then introduced to [[Otto]], [[Grimbol]], and [[Achilles]].

## Set off
we set off away from [[Tarnhold]] we can see the moonstone above us, but aside from this, it is too dark to see past our darkvision.

[[Oocelazok]] acts older than he really is, or at least, he is more capable than his age would make him seem.
[[Spark]] has spent a lot of time on boats, so feels very comfortable
[[Beast]] likes the calm of the moonstone sky
[[Selkie]] has felt very happy from the start of the journy, and seems very at home after returning to the ship. She is sucking up to the captain. 

as we set off, we open the letter. There isn't much written on it. The majority is taken up by a sketch instead. 
as we read the text of it,
This will likely require 2 or more to carry. the key will be stored in the captains quarters  or another high ranking officer. if it is open already, flee. It is a large stone [[Stone Coffin]] with chains. 

When we arive at the derelect ship, we row onto it, but it was a pain. As we pop over the deck, we see 3 corpses on the dek, and a large splattering of blood on the deck. 