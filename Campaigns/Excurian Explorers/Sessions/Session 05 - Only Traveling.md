---
campaign excurian-explorers
---
#excurian-explorers 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## Run Away
it's dark and a silence order is given on the ship
the cannon shots are always in the exact pattern, and with the same cadence
[[Spark]] is watching the other ship
[[Beast]] and [[Selkie]] help the crew on the ship
[[Oocelazok]] goes below deck with [[Ridley]]

This pursuing ship gets a bead on us, and starts hitting near us.
[[Selkie]] and [[Oocelazok]] go to put out the lights, and [[Beast]] mans the mast
[[Selkie]] jumps overboard? IDK why
There is a small lump of corally stuff stuck to the board, like a barnacle or something. bioluminescence. 
it's glowing and probably letting the other ship track us.
[[Selkie]] clears the barnicles off, and we get away

## After the Ship
### Zone of Truth
[[Oocelazok]] casts a zone of truth so they can put [[Captain Tavir]]'s mind at ease
the [[The Wwwwhimsy]] is a [[House Ralek]] ship, and they were testing, meaning they were testing on their own people
[[Ridley]] admits to killing someone
[[Captain Tavir]] asks are any of you under it's influence right now?
[[Oocelazok]] attempts to say no, but there is a brief moment where he feels like he can't, then they both answer no
[[Ridley]] also says no

[[Captain Tavir]] says he knows the employer, and they probably won't use this for evil, so it is ok to bring this to them. 
##  Shenanigans
[[Otto]] and [[Beast]] go on a bender and fix everything on the ship
[[Ridley]] thanks [[Oocelazok]] for taking him as his ward, and saves his life
[[Spark]] takes the first watch and [[Achilles]] thanks him for his work earlier while they were escaping from the other ship.
[[Oocelazok]] says, he lost his family to political things, and that he hopes [[Ridley]] has at least some fond memories of his parents. [[Oocelazok]] says, I try not to let life be lost when it can be saved. 

### Oocelazok's Dream
The [[Stone Coffin]] lady intrudes on [[Oocelazok]]'s dream, they talk briefly, and then it leaves
[[Oocelazok]] dozes off to sleep and dreams of the life he could have had with his family. While this happens, his dream glitches, and he sees a pale figure at the edge of his dream. More than pale, like no color at all. the area around them also has the pigment leached out of them for a second.  [[Oocelazok]] approaches it.
It speaks. "this I was not expecting"
This was the creature was who appeard to [[Oocelazok]] as his wife
he asks what it is
it repplies "I am currently nothing..."
it says it was the creature in the [[Stone Coffin]] "I have never force anyone to do anything they did not chose to do"
"I am not used to being perceived in this space. it was a pleasant dream"
"an old man has had time to come to terms with his losses"
it leaves