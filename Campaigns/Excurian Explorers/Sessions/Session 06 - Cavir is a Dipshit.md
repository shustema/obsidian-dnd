---
campaign excurian-explorers
banner: "![[heral.png]]"
banner_y: 0.416
---
#excurian-explorers 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```

## The Lock is Opened
we all make wisdom saving throws
### Something is wrong
#### [[Beast]]
[[Beast]] awakens with some sort of pressure and feels like something is horribly wrong [[Otto]] is asleep, but it is restless
[[Beast]] goes over and wakes him up, he had a bad dream but can't remember
[[Beast]] doesn't know what it is that is wrong, he can smell something, but it doesn't make sense. He can't place the smells and sounds, but it doesn't make sense
they set off in the wrong direction to try to find [[Oocelazok]] to see if he knows whats happening

#### [[Oocelazok]]
[[Oocelazok]] wakes up after his dream. It feels peaceful.  he is in a space between dreaming and awake. And then he hears a loud click, like someone has popped a latch/turned a key, sounds like it's in a cave. the sound is extremely loud, and feels like an ending. He takes 10 points of psychic damage. He is not waking up.
[[Oocelazok]] tries to wake himself up, (like sleep paralysis)
He beings to feel the edges of the dream world, and he extends his mind outwards, and forces himself awake with a strong charisma save

he goes to wake [[Ridley]] up, but he is already awake and is shivering. Everyone else is also shivering, and it begins to snow in the barracks
he also goes and attempts to wake up the others in the room as well, when he goes to shake him, there is no response

#### [[Spark]]
[[Captain Tavir]] first opens the lock, then throws his hands up, and looks confused. 
[[Spark]] yells "shut that [[Stone Coffin]] you idiot"
after nothing appears to happen, [[Captain Tavir]] begins muttering to himself in anger:
"you promised you bastard. promised"
he draws his sword and starts screaming, and attacks the [[Stone [[Stone Coffin]]]] 

### Everyone Gets to the [[Stone Coffin]]
the key and lock are glowing with a blue faint light
it is also snowing
[[Oocelazok]] and [[Beast]] attempt to grapple [[Captain Tavir]] but fial
[[Spark]] attempts to turn the key back, but it is incorporeal now
[[Khlorya]] hold persons [[Captain Tavir]]
[[Beast]] jumps on to the [[Stone [[Stone Coffin]]]] to hold it together, and it feels like it is buzzing. it almost feels nervous it shifts to anticipation
a memory comes to mind, but it's not one I remember
- it feels half rememberd
- it is my memory, but it is hazy
- brief flashes of me younger.  in this memory I am human
- in the memory I am looking down a box, wrapped in twine with a tag that says for Bjorn
- I look up and hear a voice that says go on son, you can open it. I rip the fuck out of the box
- the anticipation is the moment right before the box opens
in the back of his head [[Beast]] hears a voice say "Bjorn" with an increasingly evil sounding voice

[[Oocelazok]] attempts to lock it again
[[Beast]] notices the snow if falling very hard. The emotion of anticipation begins to change, it is no longer an emotion at all. [[Beast]] hears a woman's voice "Oh you may want to get off the [[Stone [[Stone Coffin]]]] now"
[[Beast]] tells the others, "we're waiting for something, lock it quick"

[[Oocelazok]] rolls a good intimidation check.  they key is blindingly bright at this point, and he focuses his intent, and locks the lock again almost. but just before he can, the whole world freezes. only we and [[Captain Tavir]] can move. [[Ridley]] and [[Achilles]] are frozen in place

### Not Quick Enough
the snow begins again
it turns into a blizzard, it is buffeting at us, it's a vortex
the [[Stone [[Stone Coffin]]]] cracks. the light that shined from the key now radiates out from inside
[[Beast]] feels this light more than he sees it. It burns cold, feels like he's being frozen to his bones
the [[Stone Coffin]] is almost imploding in on itself
[[Beast]] jumps off and feels alone in his own head

we are in the eye of the storm, the blizzard extends as far as we can see,  we feel if we were elsewhere, it would be much worse

the [[Stone Coffin]] disintegrates. we are looking at the absense of anything. unlight from within the remains of the [[Stone Coffin]]

[[The Figure]] from [[Oocelazok]]'s dream appears.
it is very thin, starved. pale
it is carrying a knotted staff, with pointed ears. and a calm emotion on their face
dragonfly wings extend out from it as it is settling back into reality

it speaks: 
"Thank you, oh thank you so much. it has been a long time since I've had my facilities. A long time since I've been able to speak to someone in person. I find I am somewhat taken aback in the moment. So I must say thank you. Where I'm from, there is a way of doing things. you have done me a service, and I owe you a service in return. I have a gift for you all. In return for the parts you have played for my return to the world, I offer you the gift of knowledge. And the gift of your lives"

#### [[Spark]]
[[Spark]] asks who are you
"i am a herald of things to come"

#### [[Beast]]
[[Beast]] asks "herald to what?"
"now that is a good question! I wondered which one I was communicated before. Which one of you is in the driving seat right now? oh you dont know! I am the herald to he who sent me, and the rest of my group. I am not at liberty to give his name or titles, but I can tell you the name of my companions. we are known as the gloming pact. I am the herald of the gloming pact"

#### [[Oocelazok]]
[[Oocelazok]] asks "what do the [[Gloming Pact]] represent"
![[What Does the Gloming Pact Represent]]

#### [[Khlorya]]
[[The Figure]] calls [[Khlorya]] cousin
[[Khlorya]] doesn't ask a question and just says "no, I will not negotiate with the likes of you"

#### [[Captain Tavir]]
[[Captain Tavir]] asks "do you intend to stick to the deal"
"awe..... such a wasted opportunity. Yes of course I intend to stick to the deal. not much of a deal if I back out of it is it"

[[The Figure]] says "ooh I'm caught slacking I got shit to do, I got to bounce"

[[Beast]] asks "who's Bjorn" 
"now I know who's in the drivers seat then. I could tell you, but I think it could be more fun for you to figure it out yourself"

[[Oocelazok]] does a divine sense, and senses that it is kind of undead but not really

[[Captain Tavir]] traded opening the [[Stone Coffin]] for a position of comfort in the new  world because hes a fucking weasel

before he leaves, he basically gives a dark mark to the cast out beast that he is the herald of

### Reality Returns
we speak with [[Achilles]], and he is sad that he saw this coming with [[Captain Tavir]] and never did anything about it

[[Khlorya]] leaves the party at this point