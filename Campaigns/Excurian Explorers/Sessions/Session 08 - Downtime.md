---
campaign excurian-explorers
---
#excurian-explorers 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```

## Finishing the Meeting
We are given our files, and we are offered to redact information on that file that we would like to keep secret

[[Mx. Blank]] tells us that it is vital that their involvement in this is to remain a secret.
[[Mx. Blank]] also tells us that they can provide us items that are probably meant to be secret.

We then leave [[The Alias]]

## Getting a Ship
we start a quest to get the [[The Wwwwhimsy]] and claim it as our own.

when we get to the [[The Wwwwhimsy]], all the bodies are gone, and [[Beast]] smells some ozone. 

[[Beast]] asks [[Otto]]  to join the [[Prosthetics Shop]], but he says that [[Achilles]] needs him around, and [[Grimbol]] is looking to jump ship. 

[[Marty Firhearth]] looks around the island while we are fixing the ship. She finds a cave, and finds a painted mural, that has a spiral on it. she kind of gets in a trance, and her sword cuts it in half. 

When we get back, we got to [[Scuttle Ally]] to have our ship kind of fixed up a little. We see homeless people basically called "the shipless", some of which are missing legs and arms.

We take it to [[Neptunes Bounty]] to get it scrubbed.