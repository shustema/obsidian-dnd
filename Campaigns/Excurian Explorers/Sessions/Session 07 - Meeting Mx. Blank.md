---
campaign excurian-explorers
---
#excurian-explorers 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```

### Weird Stuff
We start out looking at some weird stuff. We see crystal veins surrounding a central tower made of crystal. Surround this, there are 8 crystal towers around.  the colors in these minor towers seems to shift and flow.  The energy from the minor towers seems to flow into the central tower.

in the administration building of [[Prismhall]]
[[Marty Firhearth]] has been asked to attend a meeting with [[Prime Abjurer Redastra]].

## We Fucked Up
[[Marty Firhearth]] continues into [[Prime Abjurer Redastra]]'s office. [[Marty Firhearth]] knows this guy is super paranoid. [[Marty Firhearth]] knocks on the door, and is let in for her meeting. [[Marty Firhearth]] is being sent out to fix up our fuck up I think. The last known location of the object is around [[Tarnhold]]. They believe it was moving towards [[Tarnhold]], but they lost track of it. [[Marty Firhearth]] will be going to [[The Black Stacks]] as a descrete location within [[Tarnhold]] to begin her search.

[[Marty Firhearth]] is meeting up with [[Gaieuss]], someone she would consider a friend. [[Gaieuss]] tells [[Marty Firhearth]] that the [[Stone Coffin]] has been promised to somebody else.

### Cut Back to the Main Group
We go back to the [[Swallowtails]] to speak with [[Gaieuss]]. We discuss staves vs staffs. then tell [[Gaieuss]] what happened. [[Gaieuss]] tells us their employer usually has an interest in fey stuff, but didn't disclose how dangerous it was. [[Gaieuss]] tells us he will have to get his employer directly involved.

### Player Thoughts
[[Oocelazok]] is thinking about the people in [[Freehold]] that he needs to tell about the impending danger
[[Spark]] is thinking about the war he just got out of, and how he's only been here a few months, and it just seems chaos. This doesn't seem to be very normal, so he is a little stunned.
[[Beast]] is looking to take down a 5G tower
[[Selkie]]  is very overwhelmed by everything that happened. she is starting to think of everyone as her crew now. she hopes that everyone will say "alright that's that lets go get beers and not follow up on this". She doesn't want her friends to get hurt. 

[[Oocelazok]] tells the group he will bee moving on

## Deeper in the ship
[[Marty Firhearth]] makes a call to the [[Prime Abjurer Redastra]]. They create a conference call with [[Sythys]] and they discuss how we can fix this situtation that is "fucked". [[Marty Firhearth]] sweet talks our way out of a salary.

[[Gaieuss]] returns and offers to us to send a message to anynoe int the event we are killed by his employer. 
[[Selkie]] tells [[Gaieuss]] to tell a man in the [[Razors Edge]] that she tried. 

### Back to [[Tarnhold]]
we go back up to the [[Deck Market]] along with [[Gaieuss]].
We go over to a fish market. [[Gaieuss]] lays a silver down and says we are here to pick up a delivery. we are going to meet up with his employer

## The Employer
as we descend the staircase, something fucky happens. at a certain point, the rocking of the ship stops, and the sounds of the water also end. we are now descending a pretentious marble staircase. there is shitty abstract art on the walls. [[Mx. Blank]] is standing behind a desk.

[[Mx. Blank]] first says take a seat! Not your fault! don't worry about it! well do worry about it! but you won't be punished. She appologizes for saddling us with [[Captain Tavir]] who ruined the mission. [[Mx. Blank]] has a file on [[Marty Firhearth]]. [[Mx. Blank]] explains she doesn't trust the [[Prismhall]] with magic items such as the [[Stone Coffin]], and this is why she didn't want the [[Prismhall]] to have it and we were hired to intercept it.