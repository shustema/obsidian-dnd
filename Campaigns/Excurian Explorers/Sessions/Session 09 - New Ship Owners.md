---
campaign: excurian-explorers
---
#excurian-explorers
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## Interviews
### Meeting [[Tanner]]
We are first introduced to the new party member [[Tanner]].
[[Prime Illusionist Cosrow]] sends [[Tanner]] out to [[Tarnhold]] to check out a group of people who are tangentally related to an item of interest. (probably the [[Stone Coffin]]).

[[Tanner]] is ported into [[Tarnhold]], on an academy ship crewed by a few graduates from the school
[[Prime Illusionist Cosrow]] sends [[Tanner]] to meet up with the [[The Wwwwhimsy]] to solicit our help.

[[Tanner]] is lead down into [[Scuttle Ally]] to meet with us, and is a little worried about the people she is about to meet.

### Interviews Begin
We see [[Tanner]].  [[Marty Firhearth]] identifies her as a student from [[Prismhall]]. [[Selkie]] calls her up to be interviewed, and introduces herself. Each of us describe our characters.
[[Marty Firhearth]] asks if [[Tanner]] is from [[Prismhall]], and she confirms. [[Marty Firhearth]] says she is not actually a student, but is a reclaimer instead.

[[Tanner]] spots a group of mercenaries at the bottom of the ramp. They are calm, but dangerous looking, and are wearing uniforms. They are from the 

[[Marty Firhearth]] tells her, if she is from [[Prismhall]] she is either not graduated, and is a star student, or is graduated, and is competent anyways. 

When [[Beast]] sees the [[Black Crown]] capes, he has a splitting headache, and hears a memory of beating hooves, steal on steal, and a war cry in the distance.  [[Beast]] has a weird feeling in the back of his mind that these people mean us harm.  

### Candidates
- [[McNeil]]
- [[Zyzyyxa]]
- [[Deaf Dorian]]
- [[Cappy]]

[[Tanner]] asks what are your skills, and [[Spark]] specifies what are your main skills

### Crew
- [[Black Crown]] mercenaries, very diciplined, very competant - 8
- group of old sea dogs who left their old crew. injured vets, some old pirates. Probably bar mates - 10/12
- pretty cohesive, introduce themselves as [[The Gobbo]]. - a lot (enough)

the [[Black Crown]] tells [[Selkie]] "you are the captain of your own ship, you can make your own decisions, but I want you to know you made the wrong decision."