---
campaign: faustian-fellowship
banner: "![[homil.png]]"
banner_y: 0.676
---
#faustian-fellowship
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```

## The Escape Begins
[[Homil]] will enter the camp to begin the rescue
we fight, [[Morion]] and [[Hildria]] are both chained up, and eventually break free during the combat. 

[[Hildria]] is traveling with a child named [[Astrid]]. The child has a spectral hand named [[Argos]] that he both controls, and also leads him around almost like a parent would.

The leader of the bandit camp is holding the old sword that [[Homil]] recognizes as [[Vaal]]'s old weapon.

## The Aftermath
[[Morion]] explains the people we killed were his old platoon and they were abandoned mercenaries. He explains, before they mutanered, they had been hired to run guard duty on an expedition, but the researchers were overtaken by some force and fell into a hedonism. He disagreed with the actions of his second in command, and for this he was bound in those chains, and was forced to watch as his powers were used to kill hundreds of people. and asks if we would join him to defeat the cult that was formed around this. 

[[Homil]] reveals himself

### Interrogation
[[Hildria]] interogates the one remaining guard. there are auras around all the bodies, and they seem to be possessed or something. the one living guy seems angry at the second and command, and is hopeful that the captain ([[Morion]]) still lives. [[Morion]] takes command of the one remaining mercenary, and tells him to protect the woman and children. he mentions that the camp is not safe. 

The party decides to help the refugees to safety. 