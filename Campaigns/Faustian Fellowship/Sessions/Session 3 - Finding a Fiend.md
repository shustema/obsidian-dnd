---
campaign: faustian-fellowship
---
#faustian-fellowship
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## Speaking with the Town Guards
we meet [[Caliope (Cali)]] the captain of the guards. 
we find out that [[Caliope (Cali)]] has been investigating the disapearance of children for a while now, and is looking for the same thing we are effectively. 
[[Homil]] tells her that [[Lucial]]'s children [[Eulia]] and [[Aria]] were infected by a creature which is now on the run in the city.

[[Hildria]] explains what the creature looks like
[[Homil]] tells her the symbol is of an old religion. [[Hildria]] notices this symbol is a mash up of the symbols for decay and growth. 

## Investigation Begins
[[Caliope (Cali)]] stations 3 guards to assist in the investigation, one of which is an [[Eleanoura The High Inquisitor]]. 
[[Caliope (Cali)]] is on a war path.

### Crossing the bridge
we get to the bridge. The guards on the bridge are wearing a different garb, almost as if the two sections of the city are seperate cities. 

the guards do not let the party cross, and [[Caliope (Cali)]] says we may need to find unsavory means to get into the city.

### Returning to the Inn
when we return to the inn, the [[Eleanoura The High Inquisitor]] has caused issues, [[Morion]] is protecting [[Lucial]]'s unconcious body, and [[Argos]] is protecting the three children.

the [[Eleanoura The High Inquisitor]] dispels [[Homil]], so he punches her, and we fight
[[Morion]] is in a puddle of blood after being dispelled, and there are specks of gold in his blood. 

[[Hildria]] splits the [[Eleanoura The High Inquisitor]]'s mask, and the dispell drops. the [[Eleanoura The High Inquisitor]] just has a blank expression upon losing her mask. [[Hildria]] has an impossibly large grin on her face

dispite the zone of silence, we hear her word. "vile dead, you will rest".
she has 3 remaining runes:
- amulet: radiance and purity
- rosary: glowing runes
- robes: shroud of silence

We beat this bitch up.

the [[Eleanoura The High Inquisitor]] is an asimar, and her wings bust out as we knock her out 
[[Hildria]] talks to the [[Eleanoura The High Inquisitor]], while [[Golem]] tries to wake up [[Astrid]].

The [[Eleanoura The High Inquisitor]]is covered in old scars. there are also tiny brands on her body. [[Hildria]] does not know what clan of asimar she is from. 

[[Golem]] and [[Homil]] manage to wake [[Astrid]] back up, by feeding her milk. [[Astrid]] wakes up in an panic, and says one word, "Auntie"

[[Golem]] will then wake up [[Morion]]

### Aftermath
#### [[Hildria]] and [[Morion]]
[[Hildria]] pulls [[Morion]] into another room to speak with him. 
[[Morion]] says he can get some blood from the stash he gaves [[Eulia]] and [[Aria]], to get him by until he can get more. [[Hildria]] applauds him for holding off the [[Eleanoura The High Inquisitor]] for a time before we arrived. 

[[Hildria]] basically tells him that she has decided he is not a threat for the time being, but not to make her regret that decision. he says he doesn't know why they are kidnapping people, turning them into monsters. 

[[Morion]] says when it got quiet, it felt like all of his strength had been pulled out of that spot. [[Hildria]] tells him we will figure something out, something to eat. He says, given how it works, he thinks its a "leash", but it didn't compel him to do anything, but it put him down. 

[[Hildria]] says she has a "meeting" to take. and they both say they have suspisions about who she is. [[Hildria]] says she is sensing a pattern with [[Morion]] and the people with the [[Eleanoura The High Inquisitor]].  [[Hildria]] asks how [[Morion]] knew her. He says, she found me after I turned, and eternity is a long time. 

#### Inspecting the [[Eleanoura The High Inquisitor]]
the [[Eleanoura The High Inquisitor]] feels similar to my boss, but unbalanced. There are smouldering brands on her right arm where the rosary was, and that same inky darkness is flowing out of the burns and dissapating before it hits the ground. 

[[Homil]] casts identify on the rosary. each bead is cold-iron, the same stuff that was used to bind [[Morion]] in [[Session 1 - Great Escape]]. This rosary, similar to those chains are used to bind planar extremes. these creatures are more in line with feinds, than teiflings and asimar. 

The [[Eleanoura The High Inquisitor]] and [[Morion]] are not as they appear.  This object forcefully binds a spirit to listen, and they are under the command of whoever places it on them.  The beads and the chains are made specifically for the entity they are meant to hold. They cannot be reused.

Each bead is forged while still hot, placed on her arm, while threaded into the next one. 

[[Homil]] does not recognize the name of the person the symbol on the amulet belongs to, but he recognizes it.  The symbol is on an empty throne next to the lady of pain.

#### Grouping Up Again
[[Hildria]] sings a lullaby from a long time ago to [[Astrid]]
the music box only plays music while [[Astrid]] is holding it. 

[[Homil]] asks [[Caliope (Cali)]] if inquisitors usually have rosaries, and she says it's possible. [[Homil]] asks if there is ever a time when the bracelet could have been put onto her. 
