---
campaign: faustian-fellowship
---
#faustian-fellowship
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## Questioning
[[Caliope (Cali)]] asks to question the [[Eleanoura The High Inquisitor]] and [[Morion]]. [[Homil]] warns her that [[Morion]] is a friend. 

[[Caliope (Cali)]] asks [[Morion]] what happened, and why did the [[Eleanoura The High Inquisitor]] lose her mind.
[[Morion]] explains that it may be because [[Eulia]] and [[Aria]] are no longer human, but also because she seemed to be under someone else's control. 

[[Morion]] says he knew her a lifetime ago, but that story is her's to tell, and doesn't matter now because they are different people. All the knows now is that an old friend, and someone he trusted, had the eyes of an animal.

[[Morion]] says he knows what is happening. He was chained to a pedistool and forced to murder thousands. he doesn't know who is doing this, or what their goal is, but he thinks they are playing around with powers they don't understand, and that they are trying to play God. 

## To the Docks
We begin to investigate the dock. [[Hildria]] notices the sky tear open, and points it out to [[Homil]]. 
we see an iron box apear out of the sky, and falls into the ocean. The tear goes away. 

along one face of the box, there are large circular rivets. there look to be a few hatches on that side, and on the connecting sides, along with odd markings. The markings are completely outside of understanding.  They look decorative. 

[[Golem]] sees a image of a strange individual drinking poison. It does kind of remind me of mountainous night shade, used to disorient or numb victims.

[[Hildria]] sees a series of lights that seem like flashing warning lights. There is some kind of announcement that sounds like "stand aside, caution"

It's a fucking train