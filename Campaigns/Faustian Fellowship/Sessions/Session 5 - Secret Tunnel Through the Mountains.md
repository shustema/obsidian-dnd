---
campaign: faustian-fellowship
---
#faustian-fellowship 
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```

## Into the Tunnel
[[Hildria]] uses eyes of the graves, and sees a mass of bodies at the bottom of the sewers [[Golem]], and she also sees a merfolk that is worriedly looking for someone. it is odd for a merfolk to be act worried, they are usually very carefree.

as we run through the sewers, scared, [[Hildria]] grabs the merfolk, and it looks like her.  to [[Homil]], the creature looks like [[Eska]]. [[Golem]] and [[Astrid]] can't see it at all. 