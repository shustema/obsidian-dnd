---
campaign: faustian-fellowship
banner: "![[homil-formless.png]]"
banner_y: 0.248
---
#faustian-fellowship
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```

## The Long March
we get stopped trying to cross a river 
[[Homil]] will create a bridge, and after some deliberation we cross.
[[Hildria]] pulls [[Astrid]] away from [[Golem]] as he creates a sandcastle and creates wind to blow down the bastions. 
[[Homil]] asks her what her relationship is with [[Astrid]] and [[Hildria]] says, she is like a daughter to her now. 

We stop to make camp, and [[Hildria]] casts eyes of the grave, and notices there are 3 undead nearby.  [[Vincent]] is undead, there is an aura of anger from [[Morion]]. In the direction of [[Golem]] There is a sense of longing. Not sad, but ambitious. 

### Setting Up Camp
[[Hildria]] asks [[Morion]] what he is, and he responds, he made a pact with something, and he survived, but he doesn't know what he made a pact with. 
She asks [[Morion]] if he is able to control himself when he enters his "rage" state. He says no, but normally yes. He says, he has centeries of blood on his hands, and he will stomp these people. He will get his vengance. 

[[Morion]] explains he needs to feed off of life essance, and the sooner we find this cult, the sooner he will no longer be hungry. 

In the mean time, [[Homil]] and [[Vincent]] are talking about [[Vincent]]'s allegience, and what his intentions are. [[Vincent]] basically proves he is a friend to the party. 

## Making / Breaking Camp
[[Hildria]] gives [[Homil]] some coffee, and he gets very hyper. [[Golem]] spends the night pissing off the negative nancy's

in the morning, [[Hildria]] will take [[Astrid]] away from the cape, and take [[Astrid]]'s cape off, and her mask off so they can watch the sunset. She will also give [[Astrid]] a music box to play with while they watch the sunset. [[Hildria]] will join in dancing and laughing with [[Astrid]]

## Reaching the City
we part ways with some of the civilians, but the [[Orphan Child]] stays with us. They have jewel tones on their fingernails, and their eyes are viberant green.  [[Hildria]] invites the kid (who can't talk) if they would like to stay with her and [[Astrid]] for a little while. 

When they reach [[Lucial]]'s inn, [[Homil]] will help to mend and clean up some of the inn. [[Homil]] will light all the torches, but one fails to light. [[Lucial]] will light this torch by hand. [[Lucial]]'s tavern is called [[Izzies Retreat]].

We run into a demon, and he captures [[Lucial]]'s 2 daughters. he says, he will release the children, if we allow him to leave. The demon's name is [[No Name Chad]].

We defeat the [[No Name Chad]] but the 2 kids are cursed or something, and they try to bite [[Hildria]]. [[Homil]] tracks [[No Name Chad]] and we learn he is going north-west

### Investigating the Area
[[Astrid]] tells us that the [[Orphan Child]] is gone, and [[Vincent]] has no memory of this kid at all.

we find a million dead souls, and then [[Hildria]] pulls off her mask. 

There is a circle of runes, that are wrong. [[Homil]] takes these runes down. As [[Homil]] knows it, these runes would normally be part of the binding seal. This is similar to the magic that makes Eska's crystals.  The runes describe curses now. 