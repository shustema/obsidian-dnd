---
campaign: faustian-fellowship
banner: "![[Argos_01.jpg]]"
banner_y: 0.552
---
#faustian-fellowship
#session-notes 
## Overview
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Players",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "plc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "NPC's",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "npc")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Locations",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "location")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Monsters",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND contains(type, "monster")
```
```dataview
TABLE WITHOUT ID
    link(file.link, title) as "Other",
	location as "Location"
WHERE contains(this.file.outlinks, file.link)
  AND !contains(type, "person")
  AND !contains(type, "location")
  AND !contains(type, "monster")
```
## Mermaid Fight
[[Homil]] is fighting a creature named [[Isolation]].
This creature looks like [[Eska]]. This creature represents the isolation he feels
[[Hildria]] sees a creature that represents the weight of her actions, and the regret she holds. 

The creature burns both [[Eska]] and [[Hildria]]

### [[Hildria]]
she is feeling anger, but her grip loosens on the [[Isolation]]. 
[[Isolation]]: ...
[[Hildria]]: "What do you want, I thought I put that behind me."
[[Isolation]]: .... 
[[Hildria]]: "No.... you are the reminder of everything... you are my punishment, my life, my everything. but if I could go back and change things... I think I would. Right now I have to make things right, so yes, you are. You will always be worth it."
[[Isolation]]: ...
[[Hildria]]: "I see.... I can't keep it, but I promise you I will succeed, and I will keep my promise. So, I didn't think this is what you would actually look like. I had my suspissions, but this is interesting"
[[Isolation]]: ... 
[[Hildria]]: "What is it that you really want, and not just this thing before me. *takes off mask* I still see you."
[[Isolation]]: ... 
[[Hildria]] *puts mask back on* "I see. I've underestimated my enemy. if you are my enemy. I could consider you one, however to answer your statement, the only regret I hold is not facing up for what I did sooner. You're going back. But who knows, maybe someday I can face that head-on. but right now, I have more pressing matters to attend to." "I'm starting to think there is more to that person than meets the eye. Well done. Lets see if you are a good enough of a guardian for both this place and her."
[[Hildria]]: "Even though you are not the real thing, this will be an interesting battle until we meet again."
[[Hildria]]: "I was a coward is that what you want me to say? Everything I did, I thought I was doing for you. But I wasn't I did it for me. I'm so sorry. Forgive me"

### [[Homil]]
[[Isolation]] ([[Eska]]): "are you sure you're doing the right thing?"
[[Homil]]: "I've never known, but I do what I'm told. I hope that's enough."
[[Isolation]] ([[Eska]]): "will these companions feed my corpse too?"
[[Homil]]: "Perhaps. As many as it takes. I'd burn the earth. I'll repent when all is done, but for now, I can only *keep moving*. You're gone. This is a mockery"
- [[Isolation]] turns into [[Vaal]]. "How dare you" [[Homil]] responds.
[[Isolation]] ([[Vaal]]): "Do you even know where I went?"
[[Homil]]: "No.... no I don't, and it's irrelevant. You were just an obstacle placed along my path. There were many before, and many after.  Our lives are incidental."
[[Isolation]] ([[Eska]]): "Your lady would say otherwise"
[[Homil]]: "Our lady would not. And you know this... or maybe you don't. Mockery. What are you really."
[[Isolation]] ([[Eska]]): "Kiin. and you, Equilibrion, are to be tested. Stand now, and pass by your own merit."
[[Homil]]: "Then let us begin, kin"
[[Isolation]] (undead Val): 
[[Homil]] attacks. 
[[Isolation]]: "You may act once more. tell me, do you truely know what Our Lady wishes of us? Do you know why we are made?"
[[Homil]]: "I know only what I need to know. Maybe I know enough, maybe I do not. Should I ever need more information, I trust it will be provided. We are made to serve the world. It appears you have strayed from this purpose. Allow me to realign your thinking."

"Tell me, have you ever seen the thrown beside our lady's"
"I have not" 
"Seek your goals, but bear the weight of your actions. The passing of life does little to justify the bringing of new"

### The Test Continues
The creature speaks to both of us: One of your kin has passed. We shall see if I find you wanting.  
Those who are tested Need only concern themselves with what is before them. 

Doorways to the hallway appear and disappear.  This is the palace of doors, domain of our lady of pain. 

as the creature fades, "you may pass, through you bear the pain of which you've brought"

We return to the bridge, and the bridge has melted into a single plate of slag. [[Argos]] is gone, and [[Golem]] is comforting [[Astrid]].

[[Hildria]] says to [[Astrid]]. "I took so much away from you, I'm so sorry. I should have let you go, I'm so sorry. I couldn't let you go."

[[Hildria]] finds out that [[Golem]] is a bunch of lost souls keeping [[Eska]] alive, and confronts [[Homil]]
she then looks to [[Astrid]] who is smiling but her face looks empty, there is nothing in her eyes. 

[[Hildria]] will cast speak with dead, and look back at [[Astrid]].
"Are you angry with me that I didn't let you go that day?"
[[Hildria]] prays, "I don't know what to do. I took this on as a punishment. I don't know what to do."