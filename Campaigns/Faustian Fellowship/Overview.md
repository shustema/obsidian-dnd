## Campaign Overview
The party is exploring an unknown world, with loose history tied to the [[Campaigns/Nori Maidens/Overview]]. The campaign is about investigating a cult in which people are being captured and converted into demons. 

### Party Quests
- [ ] Stop the cultists that captured [[Hildria]] and [[Morion]] in [[Session 1 - Great Escape]]
- [ ] track down [[No Name Chad]] who is moving north-west in [[Session 2 - Arriving at]]

### Unanswered Questions
- What did [[Hildria]] see in [[Session 5 - Secret Tunnel Through the Mountains]] when she picked up the mermaid [[Session 5 - Secret Tunnel Through the Mountains]]
- how did [[Astrid]] die, and how was [[Hildria]] responsible as seen in [[Session 6 - No Friendly Faces]]
### Personal Quests
- What is up with the torch in [[Lucial]]'s inn
- What are the symbals that mimic undead spells, and why were they painted in [[Izzies Retreat]]

## Links
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Extras",
  description as "Type"
	
WHERE campaign = "faustian-fellowship"
  AND contains(type, "extra")

SORT file.name ASC
```
### Campaign
[[Campaigns/Faustian Fellowship/Sessions/Overview]]
[[Campaigns/Faustian Fellowship/Quotes]]
### World
[[Worlds/Humblewood/Overview]]
[[Campaigns/Faustian Fellowship/Characters]]