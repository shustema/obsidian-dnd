# Obsidian Repo for My Ongoing DnD Campaigns
## Overview
This repo contains plugins and assets, as well as a series of templates to properly organize a new or existing dnd campaign.

# Current Campaigns
[[Campaigns/Nori Maidens/Overview]]
[[Campaigns/Excurian Explorers/Overview]]
[[Campaigns/Faustian Fellowship/Overview]]
