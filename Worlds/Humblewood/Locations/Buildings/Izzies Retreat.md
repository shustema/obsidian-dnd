---
world: humblewood
type: [location, building, tavern]
location: emerald isle
---
## Overview
[[Lucial]]'s tavern
Was torn apart upon the party's first arival. 

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = humblewood
  AND contains(type, "location")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = humblewood
  AND contains(type, "npc")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```