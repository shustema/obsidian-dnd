## Map 

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Factions"

WHERE world = "humblewood"
  AND contains(type, "faction")

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Regions"
	
WHERE world = "humblewood"
  AND contains(type, "regions")

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Cities"
	
WHERE world = "humblewood"
  AND contains(type, "city")

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = "humblewood"
  AND contains(type, "npc")

SORT file.name ASC
```
### Overview