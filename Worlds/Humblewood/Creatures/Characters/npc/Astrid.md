---
world: humblewood
type: [person, npc]
faction:
location:

Gender:
Level:
Class:
Race:
campaign:
- faustian-fellowship: friendly
---
## Summary
[[Hildria]]'s kid or ward
is [[Hildria]]'s niece, but somehow [[Hildria]] is at least partially responsible for her death

is undead, and can only be spoken with if speak with dead is cast.

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```