---
world: humblewood
type: [person, npc]
faction:
location:

Gender:
Level:
Class:
Race:
campaign:
- faustian-fellowship : hostile
---
## Summary
This creature looks like [[Eska]] to [[Homil]], but looks like someone else to [[Morion]] and [[Hildria]].

Weilds a spear and axe that seem to be molded from branches with briars and thorns sprouting out of it. 
The spear and axe look blighted.
has red autum leaves growing off of it.

### Aliases
Isolation
Commitment
Loss
Duty

### Forms
[[Eska]]
[[Vaal]]
Undead [[Vaal]].

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```