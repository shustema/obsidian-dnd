---
world: humblewood
type: [person, npc]
faction:
location:

Gender:
Level:
Class:
Race:
campaign:
- faustian-fellowship: hostile
---
## Summary
Has magic items on her body which she uses to create aura effects in combat. 
- amulet: hides her wings, and contained her energy
- rosary: darkness (probably mind control)
- mask: dispel all magic
- robes: shroud of silence

she has scars all over her body, and brands.
the rosary is made out of cold-iron, and underneath it, and left small brands on her arm from where the metal touched her.

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```