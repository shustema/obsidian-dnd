---
world: humblewood
type: [person, npc]
faction:
location:

Gender:
Level:
Class:
Race:
campaign:
- faustian-fellowship: friendly
---
## Summary
Inn keeper of [[Izzies Retreat]] that gives the party board for a week as payment for safely taking her across the desert. 
She has 2 daughters, [[Eulia]] and [[Aria]]

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```