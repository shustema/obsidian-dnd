---
world: humblewood
type: [person, npc]
faction:
location:

Gender:
Level:
Class:
Race:
campaign:
- faustian-fellowship: friendly
---
## Summary
daughter of [[Lucial]]
was cursed by [[No Name Chad]]

### Characteristics
- has horns now
- skin is a burnt gold color

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```