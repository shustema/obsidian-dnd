---
world: humblewood
type: [person, npc]
faction:
location:

Gender:
Level:
Class:
Race:
campaign:
- faustian-fellowship: friendly
---
## Summary
- has multicolored gem like fingernails, and vibrant green eyes
- doesn't seem to be able to talk. 
- unaturally warm, but her temperature seems to match her mood. 
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```