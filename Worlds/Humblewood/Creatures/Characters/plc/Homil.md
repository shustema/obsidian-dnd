---
world: humblewood
type: [person, plc]
faction:
location:

Player: Mike
Gender: male
Level: 10
Class: [wizard, fighter, warlock]
Race: construct
---
# Homil the Equilibrion
![[homil.png]]
## Summary
### Appearance
- Adaptable physical form constructed from natural elements.
- Can shift and transform into different materials such as rocks, trees, sand, and water.
- Initially takes the form of an owl familiar.
- Glowing, ethereal eyes that reflect the phases of the moon.
- Delicate and intricate patterns etched into his body, resembling celestial symbols.
- Hair and features resemble the color and texture of the natural elements he embodies.
- Carries an aura of tranquility and calmness around him.
- Carries a staff adorned with lunar symbols, glowing softly at night.
### Character Traits
- Charismatic and wise, possessing a deep understanding of balance.
- Intelligent and clever, compensating for physical limitations.
- Committed to protecting and preserving the natural order.
- Affinity for the moon cycles, drawing inspiration from their ethereal presence.
### Story Beats
- Former familiar of Eska, a cleric dedicated to the moon spirit.
- Received a divine message that Eska no longer required guidance.
- Constructs a physical body from natural elements in Humblewood.
- Pupates his body, transforming into various forms using rocks, trees, sand, and water.
- Origin tied to a mysterious pact made with the Lady of Pain.
- Inherits charisma and intelligence from the pact.
- Embarks on a mission to maintain balance and resolve conflicts in Humblewood.
- Deep connection to moon cycles, seeking to uphold their influence.
- Longs to uncover the secrets of his own origin and the pact's implications.

### Personality
Homil exudes an aura of calm and tranquility, a testament to his understanding of balance and the interconnectedness of all things. His demeanor is charismatic, drawing others to his presence like moths to a flame. Although not blessed with great physical strength or speed, his wit and cleverness compensate for his physical limitations. Homil possesses a deep sense of duty to protect and preserve the equilibrium of the world.

Motivated by a strong affinity for the moon cycles, Homil embraces the natural order and seeks to maintain harmony in all aspects of his life. He finds solace in the ethereal glow of the moon and draws inspiration from its constant presence in the night sky.

## Background
### Origin
Long ago, in a previous campaign, Homil existed as the loyal and watchful familiar of Eska, a devoted cleric to the moon spirit. Homil's purpose was to guide and safeguard Eska, ensuring that he stayed true to the path and fulfilled his duties as a cleric. As a spirit bound to the moon cycles, Homil understood the delicate balance required to maintain harmony in the world.

Over time, Homil developed a deep connection with Eska, growing fond of the cleric's determination and unwavering commitment to the moon spirit. But as the campaign came to an end, Homil received a divine message from the moon spirit itself, indicating that Eska had grown sufficiently strong and no longer needed Homil's guidance. The time had come for Homil to embark on his own mission.

Within the enchanting woodlands of Humblewood, Homil took on a new form, constructing a physical body from the natural elements around him. Drawing inspiration from his former owl familiar form, he embarked on a process of metamorphosis, pupating his body to become a being of balance and wisdom. With each transition, his form shifted, allowing him to adapt and take on the characteristics of rocks, trees, sand, water, and more.

However, Homil's origin was not purely of his own making. He was the result of a pact made by another individual with the enigmatic Lady of Pain. While the details of this pact remain a mystery to Homil, he carries within him a subtle connection to otherworldly forces that grant him charisma and intelligence. 

### Motivations
Homil's newfound mission is to explore the vast woodlands of Humblewood, using his unique ability to shape-shift and his innate understanding of balance to resolve conflicts and maintain harmony. He seeks to uphold the natural order, keeping an eye on the delicate balance between light and darkness, civilization and wilderness, and other opposing forces that threaten to disrupt the world's equilibrium.

As he embarks on his journey, Homil is eager to learn more about his own origins, the enigmatic pact made with the Lady of Pain, and the implications it holds for his future endeavors. He understands that the answers he seeks lie within the mysterious depths of the world, waiting to be uncovered.

With his charisma, intelligence, and unwavering commitment to the preservation of balance, Homil, the Spirit of Balance, steps into the world of Humblewood, destined to leave an indelible mark on its history and protect its delicate equilibrium.