## Overview
```leaflet
id: terignyphtos_world
image: [[terignyphtos.png]]
height: 200px
lat: 50
long: 50
minZoom: 8
maxZoom: 10
defaultZoom: 5
unit: meters
scale: 1
marker: default, 39.983334, -82.983330, [[Note]]
```

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Factions"

WHERE world = "terignyphtos"
  AND contains(type, "faction")

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Regions"
	
WHERE world = "terignyphtos"
  AND contains(type, "regions")

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Cities"
	
WHERE world = "terignyphtos"
  AND contains(type, "city")

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's",
  lower(split(file.folder,"/")[1]) as "test"
	
WHERE world = "terignyphtos"
  AND contains(type, "npc")

SORT file.name ASC
```