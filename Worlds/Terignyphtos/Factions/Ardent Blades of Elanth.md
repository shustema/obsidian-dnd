---
world: terignyphtos
type: [location, faction]
location:
---
## Overview
### Goals
- Serve the Air Elemental
- Expand their influence

### Means

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Cities",
  contains(type, "capital") as "Capital?"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND !contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "npc")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
## History
In the olden days of Terignyphtos, the Ardent Blades of Elenath began as a fanatical faction from within the elves. They preached the supremacy of the air elemental as the one true god of their people, and they saw the other races and their deities as misguided and weak. They believed that it was the duty of the elves to wage a holy war to establish the elemental's dominance over all.

As the years passed their numbers grew, and in turn so did their influence. The Ardent Blades, in an effort to maintain their territory, began to mass produce the powerful enhancer Tuska. Tuska was created from the marrow of orcish horns, being the most potent when harvested from a living orc. The elixir would grant a surge of strength and endurance unlike anything else, but would also punish the user with hallucinations and delusions, a curse which only increased the zealots' fervor for their cause.

As the Ardent Blades grew larger, their appitite for the drug only grew. As such, they embarked on a horrific campaign of tremendous scale. They would often raid orc settlements and slaughtering them mercilessly, farming the tusks of the fallen to produce ever-greater quantities of Tuska. Worse still, they began breeding Orcs in pens, only to harvest their horns, then slaughter them. 

The Ardent Blades' reign of terror continued unchecked, until the orcs rallied together with other races to put an end to their crusade. The battle that ensued was one of the greatest in the history of the land, with both sides suffering heavy losses.

For days the battle raged on, the sound of clashing blades and ringing spells echoing throughout the land. The Ardent Blades fought with a fierce devotion, wielding their enchanted swords and imbued with the strength of Tuska, but enough races had banded together such that the Elanth were outnumbered, and the orcs fought with a desperation that rivaled even the strength of the Tuskan enhanced Elanth. 

Despite their brutality, the Ardent Blades were eventually all but wiped out, their numbers dwindling until only a handful of survivors remained. Those that did manage to escape were forced to flee into the wilderness, their once-great faction shattered and broken.

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```