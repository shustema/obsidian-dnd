---
world: terignyphtos
type: [location, faction]
location:
---
## Overview

### Associated Groups
[[Ardent Blades of Elanth]]

## Ideology
### Afinity
Lawful Evil

### Sister Factions


### Goals
The O'Deorain perform human sacrifices with the aim of bringing back the moon for each lunar cycle. They perform this ritual on "volunteers" that they collect from the nearest town to wherever they happen to be.

### Means
The O'Deorain posses some form of mind control magic which they use to cultivate their volunteers

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Cities",
  contains(type, "capital") as "Capital?"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND contains(type, "city")
  AND contains(faction, "odeorain")

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND !contains(type, "city")
  AND contains(faction, "odeorain")

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Members"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "person")
  AND contains(faction, "odeorain")

SORT file.name ASC
```
## History
*Depricated*
The O'Deorain are a faction of nomadic sages who have roamed the lands for centuries, living in a mobile commune and spreading goodwill wherever they go. They are skilled healers, and their knowledge of medicinal herbs and techniques is unparalleled. They believe that the Air elemental is the key to their survival and that it has the power to protect them from harm.

Despite their peaceful nature, the O'Deorain have faced many challenges throughout their history. They have been persecuted by those who fear their beliefs, and they have struggled to survive in harsh environments. But through it all, they have remained committed to their beliefs and to one another.

Only the elders of the O'Deorain know the true history of their people. It is a secret that they guard closely, for it is said that the O'Deorain do not worship the Air elemental out of love, but out of fear. It is suspected that they broke away from a more extreme faction, one that was perhaps more militant and aggressive in its beliefs.

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```