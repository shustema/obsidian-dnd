---
world: terignyphtos
type: [location]
location:
---
# Fake Items
## Potions
- Leum: 
	- This is actually really bad booze, but it's sold as an antidepressant. has firefly blood in it to make it glow
- Fiosaig: 
	- This is a laxative, it's used to cleanse one's body, but just makes you shit
- Afraidíseach:
	- dried and pulverized bone marrow from "a rare beast" (rats). Said to be an aphrodisiac
- Niamh's Kiss:
	- A sweet-tasting drink made from fermented berries. It's said to induce euphoria and vivid dreams, but really it just gets you drunk.
- Aonaran's Tears:
	- A clear liquid in a small vial that's said to cure any ailment, but it's really just water.
## Medicinal Items
- Brot-eanraich:
	- a soup with whey and oats, "blessed" chilled, and stored in an ornate bottle. Drink to cleanse toxins and prolong life.
- Lusrag:
	- a charm made out of herbs. Said to ward away spirits that cause illness
- Luibh:
	- just weeds. Burn to get stronger crop yields
- Ruideal:
	- ground roots with some herbs that smell bad
- Bodach cruaidh:
	- dried skin from Eska mixed with some dried herbs, and sand. Throw on your front door to protect from unwanted visitors)
## Artifacts:
- Obair-shnaidhidh:
	- a wood carving of an "ethereal figure". Said to grant favor with the air elemental, but really it's just a carving.
- Laochra's Fury (Fake Tuska):
	- A small pouch filled with white powder that's supposed to give you strength and courage. It's really just crushed beetles.
- Banríon's Grace:
	- A small bottle of oil that's said to enhance beauty and youthfulness. It's really just vegetable oil with a few drops of perfume added.
# Real Items
## Artifacts
- A fire opal that can produce a small flame when held
- A wand made from dragon bone that can produce powerful bursts of wind
- A ring that can detect nearby sources of water
- A potion that grants temporary invisibility
- A necklace that protects against mind control

## Medicinal Items
- Willow bark tea:
	- a pain reliever and fever reducer
- Arnica ointment:
	- used to treat bruises and sore muscles
- Echinacea capsules:
	- used to boost the immune system
- Calendula salve:
	- used to treat minor burns and skin irritations
- Lavender oil:
	- used for relaxation and stress relief