---
world: terignyphtos
type: [location, continent]
location:
---
## Overview
```leaflet
id: zarphil_continent
image: [[terignyphtos.png]]
height: 200px
lat: 50
long: 50
minZoom: 8
maxZoom: 10
defaultZoom: 5
unit: meters
scale: 1
marker: default, 39.983334, -82.983330, [[Note]]
```

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE wold = terignyphtos
  AND contains(type, "location")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE wold = terignyphtos
  AND contains(type, "npc")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```