---
world: terignyphtos
type: [tavern, location]
location: reinfeld
---
## Overview
The dirty beard is a tavern located in [[Reinfeld]]
The water and drinks are known for being of poor quality, and there is unsavory activity that can be seen in the streets surrounding the tavern.
The barkeep here seems to have a particular distaste of the [[Maiden Manor]], as he sees them as pretentious.

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "npc")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```