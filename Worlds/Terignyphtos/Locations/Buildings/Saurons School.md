---
world: terignyphtos
type: [location, building]
location: [zyraphil]
---
```leaflet
id: sauronsschool_world
image: [[sauronsschool.png]]
height: 500px
lat: 50
long: 50
minZoom: 8
maxZoom: 10
defaultZoom: 5
unit: meters
scale: 1
marker: default, 39.983334, -82.983330, [[Note]]
```
# Overview
Where talented students research and learn about monsters. Only students that have proven their strength can be accepted into the school. To do this, students must present a trophy of a kill to be first accepted into the school. This trophy is shrunk down, and worn as a necklace. After joining the school, additional trophies acquired are added to the necklace, so status is measured by the number  of trophies someone has on their person.

The school is run by the headmaster [[Sauron]], who has connections with the [[Maiden Manor]]

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "npc")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```