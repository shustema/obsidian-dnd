---
world: terignyphtos
type: [tavern, location]
location: reinfeld
---
## Overview
The Maiden Manor is a tavern in [[Reinfeld]]. It is owned and operated by [[Diana]], in conjunction with the other maidens. This is considered the best tavern in [[Reinfeld]], and [[Diana]] has been shown not to like their less successful rivals at the [[Dirty Beard]].

The Manor was created by Steve from Minecraft.

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "npc")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```