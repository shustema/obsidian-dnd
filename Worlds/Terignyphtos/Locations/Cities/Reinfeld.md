---
world: terignyphtos
type: [city, location]
location: [zyraphil]
---
```leaflet
id: reinfeld_city
image: [[reinfeld.png]]
height: 200px
lat: 50
long: 50
minZoom: 8
maxZoom: 10
defaultZoom: 5
unit: meters
scale: 1
marker: default, 39.983334, -82.983330, [[Note]]
```
## Overview
Reinfeld is a large city surrounded by walls, with a single landbound entrance. There is a docks and a airship bay within the city, as well as a magical shop, and many other local attractions. This city is also home to the famous [[Maiden Manor]].

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "npc")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```