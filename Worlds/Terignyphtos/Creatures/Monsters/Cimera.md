---
world: terignyphtos
type: [monster]
location: [plains]

variants: [standard, white]
cr:
bounty:
---
## Overview

## Variants
### White Variant Cimera
![[WhiteVariantCimera.jpeg]]

A creature that posseses 3 heads, a lion, a unicorn, and an eagle, with a snakes head on the tip of it's scaley tail. The creature posseses wings, and a mysterious ooze may be found in the area around the creature. 

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```