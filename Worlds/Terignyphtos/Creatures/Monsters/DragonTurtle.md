---
world: terignyphtos
type: [monster]
location: [coast, ocean]

variants: [standard]
cr:
bounty: 70g
---
## Overview
![[dragonturtle.png]]
 Dragon Turtles are huge creatures, that have a very high AC (21 or so), and a lot of health (somewhere around 300). They hit really hard with their claws and jaws, and also have a breath attack that hits a masive cone area with a constitution save, that does fire damage. DO NOT GROUP UP.
 
 Dragon Turtles also have a 15 foot reach.
 
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```