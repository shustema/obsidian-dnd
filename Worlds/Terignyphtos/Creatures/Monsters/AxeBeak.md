---
world: terignyphtos
type: [monster]
location:

variants: [standard]
cr:
bounty:
---
## Overview
![[axebeak.png]]

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```