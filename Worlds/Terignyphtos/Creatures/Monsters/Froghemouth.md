---
world: terignyphtos
type: [monster]
location: [swamp]

variants: [standard]
cr:
bounty:
---
## Overview
![[froghemouth.png]]
Huge creature that has a lot of slime. Will try to grapple and eat anyone that is in range. If it eats you, it will deal damage to you each turn you are in it's stomach. It can eat up to 2 creatures. 

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```