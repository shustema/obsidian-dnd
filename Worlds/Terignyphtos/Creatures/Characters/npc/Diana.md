---
world: terignyphtos
type: [person, npc]
location: [reinfeld, maiden manor]

Gender: female
Level:
Class:
Race:
campaign:
- nori_maidens: neutral
---
## Summary
- Lent the party a boat so they could investigate the magic island
- got angry at the party when they blew up their hotel room
- allowed them 
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```