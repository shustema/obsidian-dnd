---
world: terignyphtos
type: [person, npc]
faction:
location: rakkesh

Gender: female
Level:
Class:
Race:
campaign:
- nori_maidens: unknown
---
![[velmija.jpg]]
## Summary
[[Yyselin]]'s sister. Is trapped in an [[Yyselin's Amulet]] that [[Yyselin]] wears around her neck
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```