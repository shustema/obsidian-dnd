---
world: terignyphtos
type: [person, npc]
faction:
location: rakkesh

Gender:
Level:
Class:
Race:
campaign:
- nori-maidens: unknown
---
## Summary
The tailor in [[Rakkesh]]

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```