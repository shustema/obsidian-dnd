---
world: terignyphtos
type: [person, npc]
location: saurons school

Gender: male
Level:
Class:
Race: half-orc
campaign:
- nori-maidens: friendly
---
## Summary
- Founder of [[Saurons School]]
- Extremely friendly, seems to be happy all the time

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```