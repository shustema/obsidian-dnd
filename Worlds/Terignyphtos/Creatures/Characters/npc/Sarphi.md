---
world: terignyphtos
type: [person, npc]
location: rakkesh

Gender: female
Level:
Class:
Race:
campaign:
- nori-maidens: hostile
---
## Summary
- Will attempt to kill [[Eska]] on sight
- Is currently having a territory dispute with a rival gang

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```