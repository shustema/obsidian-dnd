---
world: terignyphtos
type: [person, npc]
faction:
location: rakkesh

Gender:
Level:
Class:
Race: earth-genasi
campaign:
- nori-maidens: neutral
---
## Summary
Tavern keep at [[Rakkesh]]

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```