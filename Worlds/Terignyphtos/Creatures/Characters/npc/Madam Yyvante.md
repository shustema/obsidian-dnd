---
world: terignyphtos
type: [person, npc]
faction:
location: rakkesh

Gender: female
Level:
Class:
Race:
campaign:
- nori-maidens : neutral
---
## Summary
owns the fortune tellers store in [[Rakkesh]]

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```