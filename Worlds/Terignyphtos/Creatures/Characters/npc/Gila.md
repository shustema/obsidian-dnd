---
world: terignyphtos
type: [person, npc]
faction:
location:

Gender:
Level:
Class:
Race:
campaign:
- nori_maidens
---
## Summary
[[Quiver]]'s dragon.

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```