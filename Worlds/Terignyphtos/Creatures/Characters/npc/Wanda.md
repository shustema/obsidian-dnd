---
world: terignyphtos
type: [person, npc]
location: reinfeld

Gender: female
Level:
Class:
Race:
campaign:
- nori-maidens: friendly
---
## Summary
- Works at the air field in [[Reinfeld]]
- Worked with [[Yyselin]]'s father to search for someone in the desert

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```