---
world: terignyphtos
type: [npc]
location: rakkesh

Gender: female
Level:
Class:
Race: elf
campaign:
- nori_maidens: friendly
banner: "![[pyria.png]]"
banner_y: 0.54
---
## Summary
[[Yyselin]]'s moma.
First known to [[Eska]] as [[Venri Bronzeshard]]

was rescued by the [[Campaigns/Nori Maidens/Overview]] from the [[O'Deorain]], as she was meant to be a sacrifice to bring back the moon

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```