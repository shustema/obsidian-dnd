---
world: terignyphtos
type: [person, npc]
location: reinfeld

Gender: female
Level:
Class:
Race: gnome
campaign:
- nori_maidens: neutral
---
## Summary
Runs a jewlery store in [[Reinfeld]]

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```