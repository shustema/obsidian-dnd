---
world: terignyphtos
type: [person, npc]
location: [unknown]

Gender: male
Level:
Class:
Race:
campaign:
- nori_maidens: neutral
---
## Summary
- Owns a magic item shop
- knows [[Rilby Wimple]]
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```