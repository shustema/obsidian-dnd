---
world: terignyphtos
type: [npc]
location: reinfeld

Gender: male
Level:
Class:
Race:
campaign:
- nori_maidens: neutral
---
## Summary
- Owns a fine clothing store in [[Reinfeld]]
- is gay
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```