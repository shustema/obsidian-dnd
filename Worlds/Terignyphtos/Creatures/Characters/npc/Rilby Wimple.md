---
world: terignyphtos
type: [person, npc]
location: reinfeld

Gender: male
Level:
Class:
Race:
campaign:
- nori_maidens: neutral
---
## Summary
![[rilbywimple.png]]
- Owns a magic Item shop
- knows [[Borious the Glorious]]

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```