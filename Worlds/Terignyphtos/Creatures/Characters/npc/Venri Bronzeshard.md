---
world: terignyphtos
type: [person, npc]
location: rakkesh

Gender: female
Level:
Class:
Race: elf
campaign:
- nori-maidens: friendly
---
## Summary
Woman who [[Eska]] escorted out of [[Rakkesh]] to an elven sancuary in a cave outside of the city limits
is actually [[Pyria Keyfeld]]