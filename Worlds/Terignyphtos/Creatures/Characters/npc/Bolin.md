---
world: terignyphtos
type: [person, npc]
location: rakkesh

Gender: male
Level:
Class:
Race:
campaign:
- nori_maidens: neutral
---
## Summary
- Used to be friends with [[Eska]] but left after seeing his disregard for people in persuit of the greater good
- Is a (ex?)-scammer
- Did some work with [[Sarphi]]'s gang

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```