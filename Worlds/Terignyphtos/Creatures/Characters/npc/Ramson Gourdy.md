---
world: terignyphtos
type: [person, npc]
location: reinfeld

Gender: male
Level:
Class:
Race:
campaign:
- nori_maidens: hostile
---
## Summary
- Cheff in the Devil's Diner
- very mad all the time
- made at [[Eska]] because he gave him a joint and paid in change when he ate at the resturant, telling him to "chill the fuck out"

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```