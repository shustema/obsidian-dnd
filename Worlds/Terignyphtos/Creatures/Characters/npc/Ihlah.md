---
world: terignyphtos
type: [person, npc]
faction:
location: rakkesh

Gender:
Level:
Class:
Race: yian ti
campaign:
- nori-maidens : friendly
---
## Summary
helped us destroy the [[O'Deorain]].

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```