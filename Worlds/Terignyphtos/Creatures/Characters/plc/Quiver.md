---
world: terignyphtos
type: [person, plc]
faction: komodo order
location: rakkesh 

Player: timitheus
Gender: female
Level: 7
Class: ranger
Race:
banner: "![[QuiverBronzeheart.png]]"
banner_y: 0.132
---
#nori-maidens 
## Summary
- Full name is Quiver Bronzeheart
## Characteristics
## Appearance
- Cloth mask on her face, that she hides behind
- Glowing purple eyes. tied to how she summons her drake
- Always leaves her mask on
- Seems to have something in her left eye, like a pigmentation issue underneath it
### Characteristics
Really Dumb
Kleptomaniac
## Background
- born into a roaming group of half-orcs and humans. Left to join [[Komodo Order]] This is how she got her drake friend [[Gila]]. 
- She outgrew the order, and wants to move up in life.
- roaming group of half-orcs/humans
- [[Komodo Order]]
### Trophies
* a claw from a cocituce
*  something from a [[White Variant Cimera]]
