---
world: terignyphtos
type: [person, plc]
location: rakkesh

Player: karisaur
Gender: female
Level: 7
Class: warlock
Race: changeling
---
#nori-maidens 
## Summary
## Characteristics
### Appearance
* female
* purple
* changeling
* Normally takes the appearance of a short tiefling
* 4'10"

### Personality
Is intelligent, and likes to party, but doesn't want to do shady things. Refused to join Eska's enterprise
Wants to learn, and fight

## Background
### Aliases
Parties with Eska, probably buys drugs from him

### Origin
Wistaria's parents wanted her follow in their medical footsteps. To spite them, she came to the moster hunting school, because, while she still wants to learn, she doesn't want to follow in her parent's footsteps.

### Factions
### Motivations
## Relationships
### Allies
### Enemies
## Other
### Deeds