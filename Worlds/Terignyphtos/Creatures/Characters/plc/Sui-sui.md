---
world: terignyphtos
type: [person, plc]
location: rakkesh

Player: ShakenSoot
Gender: female
Level: 7
Class: ranger
Race: lizardfolk
---
#nori-maidens 
## Summary
## Characteristics
![[suisui.png]]

### Appearance
- 6 foot tall
- amber slit eyes
- horn bumps on head
- eyes don't necessarily blink at the same time
- Wears a tattered filhty hooded cloak and dirty commoner clothes and a archer cap with a warm colored feather in it
### Personality
- Reserved
- Inquisitive
- Values friendship
## Background
### Aliases
### Origin
Sui-Sui was born the runt of her clutch to a mother who was primal in her instincts, laid eggs and watched over the hatchlings but showed no affection. Different from her brothers and sisters, Sui-Sui constantly sought affection and companionship from her family but received none. One day she awakened to see her family had moved on without her, she laid underneath a great Cypress Tree for days until being found by a small ranger party, Swarmkeeper Rangers dedicated to preserving the swamp. Adopted by the Rangers and finding affinity with the mosquitoes becoming her swarm, Sui-Sui became a marksman with the longbow and a deadly whirlwind with her blades.

Sui-Sui is always ready to make new friends, and longs to have a stable, permanent family, she greets everyone she meets. She understands money very little, only knowing that it is exchanged for services and goods but not quantities or even values. She is very protective of her friends and trusting of them, even when they lie. Due to her abandonment by her mother and hatchmates, Sui-Sui becomes awkward around other Lizardfolk and resorts to her quiet and reserved state that she maintains in solitude.

### Motivations
## Relationships
### Allies
### Enemies
## Other
### Deeds