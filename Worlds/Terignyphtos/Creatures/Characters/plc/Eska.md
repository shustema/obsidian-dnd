---
world: terignyphtos
type: [person, plc]
faction: odeorain
location: rakkesh

Player: Mike
Gender: male
Level: 7
Class: [cleric, druid]
Race: eladrin
---
#nori-maidens
# Eskinola Ensalamas
## Summary
Born to the [[O'Deorain]] until the age of 16
Lived in [[Rakkesh]] for 7 years
began school at [[Saurons School]] at the age of 23

Eska will perform the [[Mooncaller's Chant#Eska's original memory of the event]] upon the night of the new moon even after leaving the [[O'Deorain]], despite remembering a corrupted version of this chant.

## Characteristics
![[Eska.jpg]]

### Appearance
* Male
* violet eyes
* 6ft 2in
* 26 years old
* Fair skin
* Very Dark Hair
- Pointed ears have been carved into rounded ears

### Personality
Has a fake persona he uses when hustling, and a real personality

### Fake Persona
* Ethereal voice
* Very flamboyant when performing "rituals"
* talks about energy

### Real Persona
* kind of brash
* maybe a little racist towards tieflings
* A shit talker, but also a coward
* Prankster
* Will lie with impunity

## Background
### Aliases
### Origin
[[Eska]] grew up as a member of the [[O'Deorain]]. Growing up, everyone [[Eska]] knew wore cloaks and masks and showed their faces to nobody. [[Eska]], knew he was an elf, but did not know the race of any of the other [[O'Deorain]]. He grew up being told that the air elemental had ordered to give up their races to dedicate their life to him. And they did dedicate their lives to him. They performed rituals, and practiced healing magic as they traveled from town to town.

Despite being told the [[O'Deorain]]were created for a greater purpose, [[Eska]] grew to question the [[O'Deorain]]'s beliefs as he aged. He didn't understand why they worshiped the air elemental, a force of nature that had never done anything for them. He also had a gnawing suspision that they were hiding something from him.

One day, the nomads he was selling religious artifacts in the town of [[Rakkesh]], [[Eska]] overheard a conversation between two strangers. They were discussing the elves and their past, a bloody, horific history that [[Eska]] had never heard before. This, [[Eska]] though, must be what the elders were hiding from him. That he was of a race of monsters, using the [[O'Deorain]] as cover. Disgusted with his people, and lineage, [[Eska]] dissolved into the crowd.

The revelation shattered [[Eska]]'s faith in the [[O'Deorain]]. He felt betrayed and angry, but he didn't know what to do next. As he struggled with his disillusionment, [[Eska]]'s life took a turn for the worse. He found it harder and harder to make ends meet, and he turned to scamming and petty crime to survive. At first, he tried to limit himself to small cons and grifts, but as he became more skilled, his scams became more elaborate.

It wasn't until he crossed paths with the drug lord [[Sarphi]] that [[Eska]] realized the true cost of his actions. [[Sarphi]] was dangerous and not someone to be trifled with. After a run in, she and her men forced [[Eska]] to flee the city. [[Eska]] the traveled to [[Saurons School]] to hopefully find a new purpose and maybe make up for the deeds of his ancestors. 

### Factions
Ex-member of the [[O'Deorain]]
His clan was a breakaway sect of the [[Ardent Blades of Elanth]]

### Motivations
Wants to hide from [[Sarphi]], and her crew.
Wants to determine the source of his and the [[O'Deorain]]'s power

## Relationships
### Allies
[[Wistaria]] Regularly buys things from [[Eska]], and parties with him
[[Quiver]] is a business partner, but [[Eska]] does not trust him very much
[[Bolin]] is his friend from [[Rakkesh]]
Familiar named [[Homil]]
![[Monkey.png]]
has 2 donkeys named Mark and Eucaliptus

### Enemies
[[Vaal]] these two don't get along, but aren't actively fighting
[[Sarphi]]

## Other
### Deeds
is the owner of [[Eskinola Ensalama's Ethereal Exchequer of Enchanted Elixirs Edibles and Embellishments]]
### Trophies
* An eye of a giant spider
* An eye from the lion's head of a [[Cimera]]
- An eye from a [[Deep Crow]]
- An eye from a giant ape
- An eye from a cyclops
- An eye from a [[Gray Render]]
- An eye from a [[Kraken]]