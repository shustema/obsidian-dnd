---
world: terignyphtos
type: [person, plc]
location: 

Player: millhaus
Gender: male
Level: 7
Class: paladin
Race: deer person
---
#nori-maidens 
# Val G'taan
## Summary
## Characteristics
![[Vaal_GTan.jpg]]

### Appearance
Vaal is an imposing figure, he is 7'2", not counting antlers. 
Char marks on his left arm, neck and chest, so wears a heavy cloak to cover himself. 
Burn scar along the left side of his face.

### Personality
* stoic, but jovial at times
* will put himself in harms way to protect someone always since he lost everyone as a kid. 

## Background
### Aliases
### Origin
The forrest he grew up in was burned down in a controlled fire, that went wrong. The grove he grew up in is cinders, he is the sole survivor from his family. 

Bullied growing up.

### Factions
### Motivations
Dedicated his life to prevent more wildfilre, so took oath of the ancients, paladin of the green

## Relationships
### Allies
### Enemies
## Other
### Deeds