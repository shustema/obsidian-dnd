---
world: terignyphtos
type: [person, plc]
location: rakkesh

Player: lizzie mac
Gender: female
Level: 7
Class: druid
Race: [half, tiefling]
banner: "![[yyselin.png]]"
banner_y: 0.592
---
#nori-maidens 
## Summary
- Full name is Yyselin Gladiolus
- has the hots for [[Eska]]
### Appearance
* has tiefling-like horns
* 5'9"
* Body is covered in burn marks, 
* Looks like her wings were severely damaged. Wings look burnt and destroyed
- She has patches of burn marks on her arms. They aren't a normal burn, but are instead burn marks in the shape of hand prints.
### Characteristics
- Is intelligent, and pretty straight laced. 
- Cares for animals, and also values learning. Doesn't really like to fight it seems
- will lose her shit every once and a while, and turn into a rage monster

## Background
- Her village on the sea was attacked by a fire elemental. She has a very big fear of fire, but wants to learn about fire elementals to prevent that from happening again
- Has a sister that got her powers from the fire elemental, and Yyselin got hers from stratos and the water elemental. 
- Her father and her sister are locked in an [[Yyselin's Amulet]] that she wears around her neck.
### Motivations
- [[Yyselin]] is looking for a way, and fighting to free her family, and to fix the wrongs she and the rest of her kin have done in the past. Now that she has freed her mother [[Pyria Keyfeld]], she is looking to extend that freedom to her sister [[Worlds/Terignyphtos/Creatures/Characters/npc/Velmija|Velmija]]  and father [[Yyselins Father]].
- [[Yyselin]] was told her sister became a cleric partially to help heal her wounds. Eska doesn't know what her scars look like, but knows she has scars. and burns on her wings. don't know what's on her arms, but it's more than just burns 

## Relationships
### Allies
[[Venri Bronzeshard]] a.k.a [[Pyria Keyfeld]] is her mother
[[Worlds/Terignyphtos/Creatures/Characters/npc/Velmija|Velmija]] is her sister
[[Yyselins Father]] is her father