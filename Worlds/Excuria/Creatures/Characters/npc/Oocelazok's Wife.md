---
world: excuria
type: [person, npc]
faction:
location:

Gender: Female
Level:
Class:
Race:
campaign:
- excurian_explorers:
---
## Summary
[[Oocelazok]]'s wife, who may be dead

When [[Oocelazok]] is making death saving throws, he hears her say "come home dear"

As [[Selkie]] revives him, it appears to him as a thick fog, with a female creature, who is pale standing before him before he awakens. She appears to be wearing chains on her wrists.
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```