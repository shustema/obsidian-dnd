---
world: excuria
type: [person, npc]
faction:
location:

Gender:
Level:
Class:
Race: half-drow
campaign:
- excurian-explorers : friendly
---
## Summary
Medic of the [[The Wwwwhimsy]]

- half drow, cheery, but look nervous. quiet and soft spoken  
- lack of skill, is an open book and is a fast book. 
- can cook a little (not really) 
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```