---
world: excuria
type: [person, npc]
location: [tarnhold, swallowtails]

Gender: male 
Level:
Class:
Race: bird person
campaign:
- excurian-explorers: neutral
---
## Overview
- has a magic eye
- is a fixer. Set the group on their first job on behalf of another party

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```