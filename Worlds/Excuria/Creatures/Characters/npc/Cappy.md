---
world: excuria
type: [person, npc]
faction:
location:

Gender:
Level:
Class:
Race: cappybara
campaign:
- excurian-explorers : friendly
---
## Summary
Mascot of the [[The Wwwwhimsy]]

- not really a person at all, is a capybara wearing a trilby. pretty deadpan
- is a chill guy, and is pretty good. personable
- soothing pressance, so medic? 
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```