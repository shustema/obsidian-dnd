---
world: excuria
type: [person, npc]
location: [tarnhold, flouriana]

Gender: male
Level:
Class:
Race: teifling
campaign:
- excurian-explorers: neutral
---
## Overview
redskinned, and wears a hood. Has horns that poke through the top of the hood.
seems rough around the edges, but is loyal to his people
also known as Mr. God
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```