---
world: excuria
type: [person, npc]
faction:
location:

Gender:
Level:
Class:
Race: dragonborne
campaign:
- excurian-explorers : neutral
---
## Summary
The head of the school that [[Tanner]] goes to. 

## Appearance
is probably a dragonborne, but looks different every day. Possibly a different race.
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```