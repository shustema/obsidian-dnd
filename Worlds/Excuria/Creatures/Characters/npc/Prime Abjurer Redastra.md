---
world: excuria
type: [person, npc]
faction:
location:

Gender:
Level:
Class:
Race:
campaign:
- excurian-explorers: friendly
---
## Summary
Head mage of [[Prismhall]] or something similar

## Appearance
- has a dark skin tone
- complex braids in their long dark hair in an intricate pattern at the top of their head
- they have gentle amber eyes, but are very bloodshot because he doesn't sleep
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```