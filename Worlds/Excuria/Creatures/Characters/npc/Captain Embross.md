---
world: excuria
type: [person, npc]
faction:
location:

Gender:
Level:
Class:
Race:
campaign:
- excurian-explorers: 
---
## Summary
Was last seen leaving The [[Razors Edge]], but hasn't been seen since.
[[Selkie]] knew this person from her childhood, or younger years.
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```