---
world: excuria
type: [person, npc]
faction: [gloming pact]
location:

Gender: he
Level:
Class:
Race: eladrin
campaign:
- excurian-explorers : hostile
---
## Summary
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```