---
world: excuria
type: [person, npc]
faction:
location:

Gender:
Level:
Class:
Race: insect-person
campaign:
- excurian-explorers : friendly
---
## Summary
Cook of the [[The Wwwwhimsy]]

- Insect person, 6ft, mandables (like a praying mantis), they bow and flourish a long cloak. has 3 ams, and a lot of knives.  is telepathic. seems confident but is difficult to read
- good with cutting things
- is a very good chef (can cut a lot with knifes)
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```