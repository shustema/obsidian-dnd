---
world: excuria
type: [person, npc]
location: [tarnhold, flouriana]

Gender: male
Level:
Class:
Race: gnome
campaign:
- excurian-explorers: neutral
---
## Summary
Grimbol is the quartermaster aboard the [[Flouriana]]. 
He is one of 3 directly under [[Captain Tavir]], along with [[Otto]] and [[Achilles]].

### Appearance
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```