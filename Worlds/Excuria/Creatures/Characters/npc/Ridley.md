---
world: excuria
type: [person, npc]
location: [tarnhold, flouriana]

Gender:
Level:
Class:
Race:
campaign:
- excurian-explorers: neutral
---
## Summary
Ridley was the only surviving crew member from the ship transporting the stone coffin
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```