---
world: excuria
type: [person, npc]
location: [tarnhold, flouriana]

Gender:
Level:
Class:
Race:
campaign:
- excurian-explorers: neutral
---
## Summary
Achilles is the handyman aboard the [[Flouriana]]. 
He is one of 3 directly under [[Captain Tavir]], along with [[Grimbol]] and [[Otto]].

seems to be somewhat friendly with [[Selkie]] after she warned him the cargo they will be getting may be dangerous, and that she (like him) has experienced the pain of losing a ship.

### Appearance
- wears a bandalier of daggers 
- wears a deep forrest green cape
- very stoic
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```