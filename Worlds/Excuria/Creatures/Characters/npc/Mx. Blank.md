---
world: excuria
type: [person, npc]
faction:
location:

Gender:
Level:
Class:
Race:
campaign:
- excurian-explorers: unknown
---
## Summary
### Appearance
- They don't really have facial features, it's almost like it's blurry. Like a harry potter secret keepers charm. Execpt for perfect slate gray eyes that don't change
- very energetic
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```