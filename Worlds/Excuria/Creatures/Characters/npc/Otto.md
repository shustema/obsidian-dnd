---
world: excuria
type: [person, npc]
location: [tarnhold, flouriana]

Gender: male
Level:
Class:
Race: otterfolk
campaign:
- excurian-explorers: neutral
---
## Summary
Otto is the handyman aboard the [[Flouriana]]. 
He is one of 3 directly under [[Captain Tavir]], along with [[Grimbol]] and [[Achilles]].

### Appearance
- leather boots that go all the way up his legs
- feathered hat
- smiles a lot
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```