---

database-plugin: basic

---

```yaml:dbfolder
name: new database
description: Characters within Excuria
columns:
  __file__:
    key: __file__
    id: __file__
    input: markdown
    label: File
    accessorKey: __file__
    isMetadata: true
    skipPersist: false
    isDragDisabled: false
    csvCandidate: true
    position: 1
    isHidden: false
    sortIndex: -1
    config:
      enable_media_view: true
      link_alias_enabled: true
      media_width: 100
      media_height: 100
      isInline: true
      task_hide_completed: true
      footer_type: none
      persist_changes: false
      content_alignment: text-align-left
      content_vertical_alignment: align-middle
  Level:
    input: number
    accessorKey: Level
    key: Level
    id: Level
    label: Level
    position: 4
    skipPersist: false
    isHidden: false
    sortIndex: -1
    config:
      enable_media_view: true
      link_alias_enabled: true
      media_width: 100
      media_height: 100
      isInline: false
      task_hide_completed: true
      footer_type: none
      persist_changes: false
      content_vertical_alignment: align-middle
  Class:
    input: select
    accessorKey: Class
    key: Class
    id: Class
    label: Class
    position: 5
    skipPersist: false
    isHidden: false
    sortIndex: -1
    options:
      - { label: "[,Fighter,Barbarian]", value: "[,Fighter,Barbarian]", color: "hsl(46, 95%, 90%)"}
      - { label: "Warlock", value: "Warlock", color: "hsl(232, 95%, 90%)"}
      - { label: "Paladin", value: "Paladin", color: "hsl(73, 95%, 90%)"}
      - { label: "[,Sorcerer,Fighter]", value: "[,Sorcerer,Fighter]", color: "hsl(167, 95%, 90%)"}
      - { label: "[,fighter,barbarian]", value: "[,fighter,barbarian]", color: "hsl(81, 95%, 90%)"}
      - { label: "warlock", value: "warlock", color: "hsl(329, 95%, 90%)"}
    config:
      enable_media_view: true
      link_alias_enabled: true
      media_width: 100
      media_height: 100
      isInline: false
      task_hide_completed: true
      footer_type: none
      persist_changes: false
      option_source: manual
      content_alignment: text-align-center
  Race:
    input: select
    accessorKey: Race
    key: Race
    id: Race
    label: Race
    position: 6
    skipPersist: false
    isHidden: false
    sortIndex: -1
    options:
      - { label: "Shifter", value: "Shifter", color: "hsl(56, 95%, 90%)"}
      - { label: "Eladrin", value: "Eladrin", color: "hsl(40, 95%, 90%)"}
      - { label: "Dragonborne", value: "Dragonborne", color: "hsl(187, 95%, 90%)"}
      - { label: "Warforged", value: "Warforged", color: "hsl(160, 95%, 90%)"}
      - { label: "shifter", value: "shifter", color: "hsl(33, 95%, 90%)"}
      - { label: "cappybara", value: "cappybara", color: "hsl(134, 95%, 90%)"}
      - { label: "teifling", value: "teifling", color: "hsl(341, 95%, 90%)"}
      - { label: "bird person", value: "bird person", color: "hsl(156, 95%, 90%)"}
      - { label: "gnome", value: "gnome", color: "hsl(355, 95%, 90%)"}
      - { label: "eladrin", value: "eladrin", color: "hsl(64, 95%, 90%)"}
    config:
      enable_media_view: true
      link_alias_enabled: true
      media_width: 100
      media_height: 100
      isInline: false
      task_hide_completed: true
      footer_type: none
      persist_changes: false
  Gender:
    input: text
    accessorKey: Gender
    key: Gender
    id: Gender
    label: Gender
    position: 3
    skipPersist: false
    isHidden: false
    sortIndex: -1
    config:
      enable_media_view: true
      link_alias_enabled: true
      media_width: 100
      media_height: 100
      isInline: false
      task_hide_completed: true
      footer_type: none
      persist_changes: false
  Player:
    input: text
    accessorKey: Player
    key: Player
    id: Player
    label: Player
    position: 2
    skipPersist: false
    isHidden: false
    sortIndex: -1
    config:
      enable_media_view: true
      link_alias_enabled: true
      media_width: 100
      media_height: 100
      isInline: false
      task_hide_completed: true
      footer_type: none
      persist_changes: false
  Location:
    input: text
    accessorKey: Location
    key: Location
    id: Location
    label: Location
    position: 100
    skipPersist: false
    isHidden: false
    sortIndex: -1
    config:
      enable_media_view: true
      link_alias_enabled: true
      media_width: 100
      media_height: 100
      isInline: false
      task_hide_completed: true
      footer_type: none
      persist_changes: false
config:
  remove_field_when_delete_column: false
  cell_size: normal
  sticky_first_column: false
  group_folder_column: 
  remove_empty_folders: false
  automatically_group_files: false
  hoist_files_with_empty_attributes: true
  show_metadata_created: false
  show_metadata_modified: false
  show_metadata_tasks: false
  show_metadata_inlinks: false
  show_metadata_outlinks: false
  source_data: current_folder
  source_form_result: 
  source_destination_path: /
  row_templates_folder: Assets/Database
  current_row_template: 
  pagination_size: 10
  font_size: 16
  enable_js_formulas: false
  formula_folder_path: /
  inline_default: false
  inline_new_position: last_field
  date_format: yyyy-MM-dd
  datetime_format: "yyyy-MM-dd HH:mm:ss"
  metadata_date_format: "yyyy-MM-dd HH:mm:ss"
  enable_footer: false
  implementation: default
  show_metadata_tags: false
filters:
  enabled: false
  conditions:
```