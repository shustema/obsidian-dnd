---
world: excuria
type: [person, plc]
faction:
location:

Player: Boz
Gender: Female
Level:
Class:
Race:
---
#excurian-explorers 
## Overview
We first meet Marty see [[Prime Abjurer Redastra]]
Marty seems to be an archeologist of some type
is a reclaimer for [[Prismhall]]

## Characteristics
### Appearance
- Wearing a leather shawl linked together with a short brass chain
- has a big rucksack with mining gear and lots of pockets

### Personality
## Background
### Aliases
### Origin
### Motivations
## Other