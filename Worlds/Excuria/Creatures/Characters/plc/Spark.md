---
world: excuria
type: [person, plc]
location: [tarnhold, flouriana]

Player: DaveG
Gender: male
Level: 6
Class: [sorcerer, fighter]
Race: warforged
---
#excurian-explorers 
## Overview
### Appearance
- 6 foot tall
- black clad, warforged
- Wears robes, which ocassionally opens to reveal a white body. He appears to either be wearing a big helmet, or is actually made of metal, but it is unclear.
- Little sparks jumping around his body naturally
- doesn't look fully male or female
## Background
Has 6 siblings, 2 of which he has meant
