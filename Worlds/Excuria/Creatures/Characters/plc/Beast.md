---
world: excuria
type: [person, plc]
location: [tarnhold, flouriana]

Player: Mike
Gender: male
Level: 6
Class: [fighter, barbarian]
Race: shifter
banner: "![[bjorn.png]]"
banner_y: 0.132
---
#excurian-explorers
## Ideas
the entity has basic sentience, and not much else, (basically raising a kid)
read iron widow
beast is vibing with the manicles on
beast and oc may get along

## Summary
- Beast does not have good memory of his past
- Has a split personality, or another entity living in his mind
- pretty nice guy when he has his wits about him 
## Appearance
- 10ft tall 
- covered in fur
- beast like creature 
- has large shackles on each wrist 
- gets slimy when he fights
## Background
Bjorn Highmoore had always been a loyal soldier of House Varyn, eager to prove his worth on the battlefield. When he was chosen to join the first battalion sent to attack the newly emerged Vaynebourne Union, he felt honored to be part of such an important mission.

Bjorn's battalion was one of the first to engage the enemy. Charging straight into the fray with swords drawn. They fought fiercely, but the Vaynebourne Union despite being ill-prepared, were not easily defeated. One by one, Bjorn's comrades fell around him, until he was the only one left.

Bjorn, determined, continued pushing - slashing - fighting - advancing. Something inside his head was pushing him -- demanding of him to continue fighting. At least Bjorn thought it was something in his head. he could feel his mind escaping from him. He could feel his faculties leaving. The battle raged on with him and a dozen soldiers, until Bjorn fumbled his sword, but this did not deter him, as he lashed out, in what he thought would be a blow, but what was instead was a slash from a set of huge claws that had grown from his hand. 

Without thinking, Bjorn fought on, using his newly aquired weapons. Though he was unable to notice, his body had twisted and been deformed by the darkness consuming his mind. He had changed. No longer a man, but a beast, galloping through the vaynbourne ranks, ripping limb from torso, relieving shoulder's of their heads, mind's from their bodies. 

Bjorn lost himself completely, as the darkness and nature overtook his mind as it had already done so to his body, and he let out a primal howl.

---
Years went by, and The Beast of Bolus became a legend, a story told in hushed whispers around campfires. Some claimed to have seen him, wandering through the wilderness with his twisted form and glowing eyes. Others claimed he had been seen leading armies of dark creatures, bent on destruction and chaos.

Bjorn himself was not sure what he had become, or what he was meant to do. He felt lost and alone, cursed by the darkness that had consumed him. But he could not deny the power he now possessed, or the hunger that drove him onward. Someone had done this to him, but for what reason, he didn't know. The world was out to get him - or had it always been? 

Bjorn wasn't sure, but he was determined to find out.
## Appearance
## Old Wive's Tale
### Baseline
Listen close, my friends, to a story most dire,
Of a knight so bold, with pride hot like fire.
His sword was sharp, his armor shone bright,
He laughed at the weak, he reveled in might.

A knight so proud - arrogance on display,
Caught the knowing eye of a crone one day.
Seein' through his facade, he was cursed with spell,
Transforming him into a monster - scales and horns as well.

With giant claws and a fearsome roar,
The knight now a creature, feared and deplored.
No longer did he boast with pride or swagger,
But instead lived in shame, an outcast,  a lurker

So heed this warning, his lesson to learn,
For pride leads to downfall, a point to discern.
The Nightly Maw, once a knight of great pride,
Now serves reminder, to all who deride.

### The Arrogant Beast

A knight so proud, arrogance on display,
Who reveled in might, and laughed at the dismayed.
His sword was sharp, his armor shone bright,
But heart was dark, fed by prideful delight.

Who one day met the crone, who saw through his facade,
And cursed him with spell, becomming a monster so odd.
With fur and horns, and giant claws,
He became The Nihtlica Mael, feared by all.

No longer did he boast, no longer did he swagger,
But lived in shame and despair, as an outcast and a lurker.
The lesson he learned was a hard one to take,
For his pride had led to his downfall, a bitter mistake.

So heed this warning, all ye who aspire to be great,
For pride can blind you and seal your fate.
The Nihtlica Mael, once a knight of great pride,
Now serves a reminder, to all who deride.

### The Servential Knight

A knight so strong, he fought with all his might,
To protect his comrades, in the battles of day and night.
His sword was sharp, his armor shone bright,
And he never gave up, always ready to fight.

One day he faced a foe, stronger than he had ever met,
And he knew that his victory was something he couldn't get.
But he refused to give up, refused to flee,
And decided to sacrifice his humanity.

He made a deal with the devil, to gain more power,
To defeat the enemy, in that darkest hour.
He became the Nocturnal Maw, with fur and horns,
But he fought with a strength, like never before.

The battle was won, his comrades were saved,
But the cost was high, as his humanity was waived.
He lived in seclusion, with no one to call friend,
But he had no regrets, for he had fulfilled his end.

So heed this story, all ye who fight for a cause,
Sometimes the sacrifice is worth more than the applause.
The Nocturnal Maw, once a knight of great might,
Now serves as a reminder, of the sacrifices made in the fight.

## Conspiracy Theories
- The Vaynbourne Union and House Varyn are secretly collaborating to stage a fake conflict in order to distract the other noble houses from their true agenda. According to this theory, the Vaynbourne Union is actually a puppet organization controlled by House Varyn, and the ongoing skirmishes on their northern border are carefully choreographed to maintain the appearance of hostility while avoiding any real casualties. The purpose of this elaborate ruse is unclear, but some speculate that House Varyn is secretly developing a weapon or technology that they want to keep hidden from their rivals.

- House Varyn is using its advanced technology to manipulate the Vaynbourne Union and other neighboring factions. This theory posits that House Varyn has secretly developed mind control devices or other forms of advanced psychological warfare that they are using to exert covert influence over their neighbors. The ongoing conflict with the Vaynbourne Union is actually a cover for House Varyn's true goal of subjugating or manipulating their weaker neighbors for their own benefit.

- The Vaynbourne Union is secretly controlled by a cabal of rogue mages who are using their powers to undermine House Varyn's military dominance. According to this theory, the Vaynbourne Union is a front organization for a group of powerful and secretive mages who are working together to thwart House Varyn's plans. These mages are using their powers to end all magic use in Excuria. They are also using their magic and trickery to sow discord among their generals, and to launch surprise attacks against key targets. The ultimate goal of the mage cabal is unclear, but some speculate that they are trying to establish a new order in which their magic reigns supreme.

- House Varyn is planning to launch a surprise attack on House Koryth with the help of the Vaynbourne Union. This theory suggests that House Varyn and the Vaynbourne Union have secretly formed an alliance to take revenge on House Koryth for the death of Princess Varyn. According to this theory, House Varyn has been secretly building up its military might and stockpiling advanced weapons and technology, all while maintaining the appearance of neutrality under the guise of the Sparkwrought Pact. The ongoing conflict with the Vaynbourne Union is actually a smokescreen for their true plan, which is to launch a sudden and devastating attack on House Koryth with the full support of the Vaynbourne Union.

- House Varyn has developed a secret weapon that can control the minds of dragons, and they plan to use it to conquer the entire realm. This theory posits that House Varyn has been secretly experimenting with dragon magic, and has developed a device or potion that can control the minds of the fearsome beasts. The ongoing conflict with the Vaynbourne Union is actually a testing ground for this new weapon, and House Varyn plans to unleash it on the other noble houses once they have perfected it.

- The Vaynbourne Union is a front for a race of shape-shifting aliens who plan to conquer the realm. According to this theory, the Vaynbourne Union is not a human faction at all, but rather a group of extraterrestrial beings who have infiltrated the realm disguised as humans. Their ultimate goal is to take over the entire realm and enslave humanity, using their advanced technology and shape-shifting abilities to maintain their disguise. Bjorn Highmoore was changed into one of these creatures. 

- House Varyn is secretly ruled by a cabal of immortal beings who have been manipulating human affairs for centuries. This theory suggests that the true rulers of House Varyn are not mortal humans at all, but rather a group of immortal beings who have been guiding the course of human history for centuries. These beings may be vampires, demons, or other supernatural creatures, and they use their long lifespans and magical abilities to manipulate the House Varyn nobles and maintain their grip on power.

- House Varyn is secretly run by a cabal of sentient swords, who control the minds of the noble house members. According to this theory, the swords that House Varyn wields in battle are not just enchanted weapons, but are actually sentient beings with their own will and consciousness. These swords have somehow taken control of House Varyn's ruling council, and are using their psychic powers to manipulate the minds of the noble house members and maintain their grip on power.

- The Vaynbourne Union is actually a group of time-traveling warforged from the future, who are trying to change the course of history. This theory posits that the Vaynbourne Union is not a human faction at all, but rather a group of advanced warforged who have traveled back in time to prevent a catastrophic event from occurring. They are waging war against House Varyn in order to ensure that their desired outcome is achieved, and will stop at nothing to achieve their goals.

- House Varyn has discovered a portal to another dimension, and is using it to recruit powerful beings from other realms to fight for them. According to this theory, House Varyn has discovered a portal to another dimension that is filled with powerful beings, such as demons, angels, and other magical creatures. They have been secretly recruiting these beings to fight for them in their ongoing conflict with the Vaynbourne Union, and have amassed a powerful army of otherworldly beings that will ensure their victory.

- The ongoing conflict between House Varyn and the Vaynbourne Union is actually a plot by a group of malevolent gods, who are using the realm as a playground for their twisted games. According to this theory, the gods of the realm are not benevolent beings who watch over the mortals, but rather a group of malevolent entities who delight in causing chaos and destruction. They have created House Varyn and the Vaynbourne Union as pawns in their twisted game, and are manipulating events to ensure that the conflict continues indefinitely.
 