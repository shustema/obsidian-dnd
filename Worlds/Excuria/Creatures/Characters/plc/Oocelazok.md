---
world: excuria
type: [person, plc]
location: tarnhold

Player: Bozz
Gender: male
Level: 6
Class: paladin
Race: dragonborne
---
#excurian-explorers 
## Overview
### Appearance
- fairly tall, 5-11 ish
- skin is white/blue
- pretty old
### Personality
- acts older and less capable than he really is to throw people off
## Background
Lost his family to political reasons
### Aliases
### Origin
### Motivations
He came to [[Tarnhold]] because this is a good place to loose yourself and start again
Tries not to let life be lost when it came be saved
## Other