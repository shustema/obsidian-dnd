---
world: excuria
type: [person, plc]
location: [tarnhold, flouriana]

Player: TashaK
Gender: female
Level: 6
Class: druid
Race:
banner: "![[Selkie_screenshot.png]]"
banner_y: 0.132
---
#excurian-explorers

## Overview
- Is the new captain of the [[The Wwwwhimsy]]
- lost a ship at some point
### Appearance
- 5ft 6inch
- sea foam hair
- skin is the color of water, and looks constantly wet
- wearing salor garb
## Background
### Origin
grew up in [[Tarnhold]]
### Motivations
There is some shit with her and captain imbross, and she received a note from her mother Sia. There are some rumors comming around that captain imbross is still alive
This is why she is returning now for the campaign
## Other
selkie has lost a ship at some point