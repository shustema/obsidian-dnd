---
world: excuria
type: [location, building]
faction:
location: Tarnhold
---
## Overview
The Wondrous World of Whimsical Wooden Wonders - Where Fantastical Forms Flourish: A Magnificent Emporium of Marvelous and Mirthful Prosthetics from the Enchanted Realms!

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = "excuria"
  AND contains(type, "location")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = "excuria"
  AND contains(type, "npc")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```