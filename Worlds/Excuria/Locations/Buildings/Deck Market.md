---
world: excuria
type: [location, building, market]
faction: house tithos
location: tarnhold
---
## Overview
```leaflet
id: worlds_excuria_locations_buildings_deck_market_md
image: [[deck_market.png]]
height: 500px
lat: 50
long: 50
minZoom: 8
maxZoom: 10
defaultZoom: 5
unit: meters
scale: 1
marker: default, 39.983334, -82.983330, [[Note]]
```
Very busy all the time. the city that never sleeps, there is no day or night in the deck market, but there is in the rest of [[Tarnhold]]. There is always stuff to buy, and always stuff to sell.

This location is static, there are so many ships tied together, so that they really can't move. Most are markets, but there are a few tavern and resturant ships.

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "npc")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```