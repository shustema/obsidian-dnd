---
world: excuria
type: [location, building]
location:
---
## Overview
The buildings consist of crystal veins surrounding a central tower made of crystal. Surround this, there are 8 crystal towers around.  the colors in these minor towers seems to shift and flow.  The energy from the minor towers seems to flow into the central tower.

In the center, there is a huge vat of energy, and a truly massive library of scrolls and books. 

Higher in the tower, there is an administration building.

### Teachers
- [[Prime Abjurer Redastra]]
- [[Prime Illusionist Cosrow]]

### Students
- [[Marty Firhearth]]
- [[Tanner]]
## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = "excuria"
  AND contains(type, "location")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = "excuria"
  AND contains(type, "npc")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```