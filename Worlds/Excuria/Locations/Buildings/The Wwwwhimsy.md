---
world: excuria
type: [location, building]
faction: house ralek
location:
---
## Overview
A direlect ship that we are sent to search by [[Gaieuss]] to search for a mysterious coffin.

originally called the Silent Sisters, but renamed to the Wwwwhimsy as the excurian explorers commandeered it. 

The figurehead of the ship is [[Captain Tavir]], and there is a chamber pot in the back of his head, that has a channel which makes him cry when you piss in it. 

[[Selkie]] is named the captain of the Wwwwhimsy

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = "excuria"
  AND contains(type, "location")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = "excuria"
  AND contains(type, "npc")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```