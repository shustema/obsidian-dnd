---
world: excuria
type: [location, city, capital]
faction: house tithos
location:
---
## Overview
```leaflet
id: worlds_excuria_locations_cities_tarnhold
image: [[Tarnhold_map.png]]
height: 500px
lat: 50
long: 50
minZoom: 8
maxZoom: 10
defaultZoom: 5
unit: meters
scale: 1
marker: default, 39.983334, -82.983330, [[Note]]
```
### Aliases
The City of Ships

### Description
The city is made up of hundreds of shipps tied together
At the center of the city, we see the grand rock citadel. 
on the top of the citadel, we see a orange pire shining light over the city. 

several ships have been essentially boxed in over the centuries. The ships at the center are a semi perminent fixture. This is the deck market. This is a  place where anyone can find anything... as long as you don't ask anyone.

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "npc")
  AND contains(location, lower(this.file.name))

SORT file.name ASC
```
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```