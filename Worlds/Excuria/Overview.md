## Map
```leaflet
id: Worlds_world
image: [[Excuria.png]]
height: 200px
lat: 50
long: 50
minZoom: 7
maxZoom: 10
defaultZoom: 5
unit: meters
scale: 1
marker: default, 39.983334, -82.983330, [[Note]]
```

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Factions"

WHERE world = "excuria"
  AND contains(type, "faction")

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Regions"
	
WHERE world = "excuria"
  AND contains(type, "regions")

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Cities"
	
WHERE world = "excuria"
  AND contains(type, "city")

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = "excuria"
  AND contains(type, "npc")

SORT file.name ASC
```
### Overview
Deep below the surface of the world there lies a cavern, a cavern so impossibly large in scale that it contains an entire sea. Cresting from that black sea there are a series of rugged islands that have never known and will never know the kiss of sunlight, and on these inhospitable islands we set our scene.

Escuria was once the seat of a mighty subterranean empire, who were united in devotion to their Empress Arachne the Great, a figure of mythical power who led her Drow brethren to these lands on a pilgrimage to found a new society. Arachne ruled her fledgling empire alongside her eight children for millenia, guiding her people into a great age of prosperity and unity. However, nothing can last forever. Even the elves with their greatly expanded lifespans must one day pass on from this life, and upon her death there was no readily apparent heir, with each child holding differing opinions on who was best equipped to sit the Arachnean Throne.

Unable to agree on a worthy candidate it was only a matter of time until the inevitable came, and soon sibling fought against sibling to take the throne by force. By the time the dust of civil war had settled only four of the heirs remained, and the survivors sought to bring stability and an ending to the war that had shattered their empire. The remaining children; Marevo, Koryth, Ralek and Varyn, agreed that none of them were currently worthy successors to their mother and came to an arrangement known as the “Accords of Old”, formally splitting the territories of the empire into what we know today as the Noble Houses. Along with this they decided that in order to bring an end to the hostilities between one another, and within the Accords of Old, they each signed a magically binding contract that prevented them from ever going to war against one another.

Whilst this has led to an age of peace between the Noble Houses, each has continued to grow their spheres of influence and push at the boundaries and loopholes of this contract over the intervening centuries, and there is still a great deal of mistrust between these old enemies. In recent years, the establishment of two new Noble Houses, House Tithos and House Ledilesi, the arrival of outsiders in the form of The Vaynebourne Union, and the rapid growth of political movements such as The Ascendancy has lead to an upheaval in the delicate balance, and as the centuries have passed the magic of the Accords of Old has also begun to weaken.

Between the nobles vying for power, the generals vying for blood and the oppressed underclasses vying for freedom, Escuria is a powder keg waiting for a match.
