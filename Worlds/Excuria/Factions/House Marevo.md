---
world: excuria
type: [location, faction]
location:
---
## Overview
### Ideologies
- Tradition
- Diplomacy
- Order
## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Cities",
  contains(type, "capital") as "Capital?"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND !contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "npc")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
## History
House Marevo was one of the original four Houses to emerge at the conclusion of the War of the Four, with their founder Prince Marevo taking the lead in negotiating the terms that brought about an end to the conflict. As such, House Marevo was able to ensure that they would be in an advantageous position as Escuria entered peacetime, not only securing some of the most resource rich areas for their territory, but also claiming the once imperial capital of Snowharbour as their own.

House Marevo quickly set about becoming an economic powerhouse, due in no small part to their extensive merchant navy, and Snowharbour being home to one of the largest marketplaces in Escuria. Following the death of Prince Marevo the throne has passed to his son Prince Varmire Marevo, but tensions between Varmire and his brother Admiral Tithos Marevo continued to fester after his ascension to the seat of power, leading eventually to Tithos’ decision to abscond with their Navy and form the rogue House Tithos. This schism, combined with the rising star of House Ledilesi in the south, has had a large impact on House Marevo and their days of economic dominance are now long behind them, though the nobility still maintain their extravagant lifestyles.

House Marevo have pivoted their focus in recent years and now regard themselves as the peacekeepers of Escuria, attempting to maintain diplomatic relations between the other Houses through establishing embassies and envoys to maintain the fragile peace. Unfortunately, most of the other Houses are indifferent to these efforts at best, or view them as intrusive and suspicious at worst. Behind closed doors House Marevo often look down on the others as childish and short-sighted, and there is an air of superiority that often accompanies a member of the diplomatic corps whilst abroad.

House Marevo prizes tradition and structure above all else, and there is a strict social hierarchy to their society which they expect all citizens to abide by. Nowhere is this more apparent than in the capital where the palace and noble estates tower above the slums below and the nobles very literally look down upon the common people below. The lavish opulence and grandeur of Snowharbour is a thin facade masking a society of haves and have-nots, and the depths of the lower city are a hotbed of political unrest.

Even within the halls of power many are jockeying for position and prestige, as the now-ancient Prince Varmire is yet to name a successor, and his choice could spell the difference between a House that maintains its traditionalist views or a more modern Marevo for the new age.

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```