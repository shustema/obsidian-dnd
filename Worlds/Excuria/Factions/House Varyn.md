---
world: excuria
type: [location, faction]
location:
---
## Overview
### Ideologies
- Aggression
- Technology
- Supremacy

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Cities",
  contains(type, "capital") as "Capital?"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND !contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "npc")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
## History
House Varyn is very much a reflection of their founder, as Princess Varyn was one of the foremost aggressors in the War of the Four and her people remain one of the most martially focused of all the Noble Houses. House Varyn prizes strength, strategy and cunning above all else and they very much subscribe to the belief that “might makes right”.

House Varyn harbours a deep distrust of magic and magic users as they see it as a way to subvert their hierarchy of martial strength, a sentiment that only grew stronger after the untimely death of Princess Varyn in the eruption of the volcano Chokemount, while on a visit to House Koryth. Many in House Varyn place the blame for this event on House Koryth and its then-leader, Princess Koryth, believing that the inaction of the powerful mages of the house caused Princess Varyn’s demise. As such magic is heavily controlled and regulated within their borders, and those considered to be “rogue mages” are quickly dispatched with little mercy.

After Princess Varyn signed the Accords of Old, the House was left in a difficult position of being an army with no-one left to fight. It seems inevitable then that after her death they would begin to fight amongst themselves. The House went through a long period of conflict known as “The Grand Shogunate Wars” before one of the warlord generals was able to amass enough support to proclaim themself the Grand Shogun of the House and claim the fealty of the other generals.

This Grand Shogun was able to declare that the capital city of Sakani was to remain neutral ground in any future conflicts, and built the impressive iron walls that still protect it to this day. Whilst nowadays there is no formal “ruler” of House Varyn, with each region instead having its own independent Lord-General, it is still common for people to informally refer to the Sword-Warden of Sakani as the Grand Shogun, and they hold a great amount of sway amongst the Lord-Generals and act as the guardian of the capital city.

Due to their refusal to employ anything but the most basic of spells and enchanted weapons in combat, House Varyn has instead relied upon their mastery of technology to stay at the cutting edge of warfare. Sakani is home to the Sparkwrought Pact, a group of scientists and industrialists who have catapulted Sakani into an industrial revolution, leading to the invention of rudimentary firearms, mass produced weapons, and combat ready automatons.

House Varyn has been able to test these new inventions out to great effect on their northern border, due to their ongoing conflict with the outsiders of the Vaynebourne Union, but it is no secret that they are waiting for an excuse to be able to break the Accords of Old and take the fight to House Koryth.
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```