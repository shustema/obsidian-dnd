---
world: excuria
type: [location, faction]
location:
---
## Overview
### Ideologies
- Commerce
- Comaraderie
- Individuality
## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Cities",
  contains(type, "capital") as "Capital?"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND !contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "npc")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
## History
House Tithos occupy a strange place amongst their peers, as they were once not a House at all, but instead the navy of House Marevo. Lord-Admiral Tithos had long chafed at taking orders from his brother, and after a particularly bad series of orders given to him at the Battle of Taveo the resentful Tithos ordered a full retreat and absconded with those ships that remained loyal to him, proclaiming himself the head of the newly formed “House Tithos”. Whilst the other Noble Houses at first refused to acknowledge his position, he began to use his unique position of being outside the magical contract to order his ships to prosecute a campaign of terror, crippling all naval trade across Escuria for several months.

Eventually a combination of this and an unlikely alliance with the Ledilesans lead to both Tithos and Ledilesi being recognised as Noble Houses in their own right and becoming signatories of the Accords of Old. House Tithos has retained their rebellious spirit even after they attained legitimacy, and take every opportunity to remind the other houses of how they came to power. The House is still ruled by Lord Tithos today under the title of Grand Admiral, and he has survived many attempts on his life from those both within his house and from abroad.

House Tithos has a stranglehold on all naval trade in Escuria, operating both the largest merchant fleet and largest pirate fleet, which has led to their capital city of Tarnhold becoming a hub of trade and commerce for both legitimate goods and the black market. In House Tithos there is no delineation between the two, as there are almost no formal laws or law enforcement outside of the military fleets which are kept under strict control by the Admiralty. House members are encouraged to police each other and resolve disputes however they see fit, meaning that often people will only rely upon their own ship's crew and those that they trust. The only real law in House Tithos is the law of profit - if you make money and pay your dues to the Grand Admiral, anything is fair game.

This leads to House Tithos being an eclectic mix of merchants trying to make an honest day's wage, Pirates taking anything and everything that isn’t nailed down, and a highly trained elite military force that are loyal only to the Admiralty Board. Some people are attracted to the house by the promise of personal freedom, others due to potential for profit, and many due to the knowledge that they can get away with pretty much anything as long as they can fight their way out. All are united however in the understanding that should Grand Admiral Tithos ever give the order to muster the fleet, every ship will follow him without question or else meet a watery grave.
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```