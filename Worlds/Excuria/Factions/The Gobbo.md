---
world: excuria
type: [location, faction]
location:
---
## Overview
small horde of goblins who fight over leadership

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Cities",
  contains(type, "capital") as "Capital?"
	
WHERE world = excuria
  AND contains(type, "location")
  AND contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE wold = excuria
  AND contains(type, "location")
  AND !contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE wold = excuria
  AND contains(type, "npc")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```