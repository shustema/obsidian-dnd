---
world: excuria
type: [location, faction]
location:
---
## Overview
### Ideologies
- Freedom
- Equality
- Revolution

## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Cities",
  contains(type, "capital") as "Capital?"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND !contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "npc")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
## History
The Ascendancy are one of the most recent players on the Escurian scene, only really coming to the fore in the last decade. They occupy a strange space, being both a fledgling nation and also a political movement. The nation known as The Ascendancy are the remnants of a rebellion who overthrew the corrupt Ledilesan nobles that had been depriving them of fair wages and humane living conditions, and seized their riches for themselves. Despite attempts by the nearby nobility to reclaim the island from the people, The Ascendancy were able to withstand the siege and there have been no further attempts to root them out from their now capital of Freehold.

The movement known as The Ascendancy is a loose group of the oppressed throughout Escuria who have been inspired by the Freehold rebellion to begin planning to overthrow those who would seek to keep them in poverty and misery. This idea has understandably spread very quickly, and there are cells of  Ascendancy agents working in secret in almost every village, town, and city in Escuria. A handful of the more remote settlements in Escuria have also come under some level of Ascendancy or Ascendancy sympathiser control, and although the establishment refuses to acknowledge them for fear of spreading the ideology faster, it is continuing to spread regardless.

At the core of the Ascendancy there is the same struggle you find within any revolutionary movement - there are those who want to pursue their goals through peaceful means and gradual change, and those who want to burn the system down today and deal with the consequences tomorrow. Trying to toe that line between the more diplomatic and the more militant sides of the movement is their elected leader Half-Paw, who maintains a council of advisers from both sides of the spectrum.

Unfortunately this fledgling nation has very little in the way of resources to be able to achieve their goals, and whilst they are currently enjoying the privilege of being somewhat ignored by the Noble Houses, it seems almost inevitable that either one of the more militant cells will make a scene too big to be swept under the rug, or else the movement will grow too large to be left alone. Until that time comes the Ascendancy continues to prepare their contingency plans, but they do have one significant advantage that cannot be discounted - In Escuria, the poor outnumber the rich.
## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```