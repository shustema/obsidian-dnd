---
world: excuria
type: [location, faction]
location:
---
## Overview
### Ideologies
- Arts
- Magic
- Espionage
## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Cities",
  contains(type, "capital") as "Capital?"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND !contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "npc")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
## History
House Koryth was founded by Princess Koryth at the conclusion of the War of the Four, and they were originally entrusted with being the stewards of The Temple of Lolth, and arbiters over all Lolth worship in Escuria. This was a solemn duty that was taken very seriously, and for a time their influence on society made House Koryth the largest landowners in all of Escuria.

This did not last however, as the eruption of the volcano Chokemount lead to a significant shift in the balance of power. With their once capital of Fort Mourne now a pile of rubble and their reputation brought into question, House Ralek came to the fore to take stewardship over a reformed and re-organised version of the Church of Lolth, and a disgraced Princess Koryth went into self imposed exile in her summer palace in Irama, which now functions as the capital city of the House . House Varyn have never forgiven House Koryth for the events of that day, as they believe that it was the inaction of Princess Koryth that lead to the death of Princess Varyn during the eruption and the tumultuous Varynian civil war that followed.

As a way of atoning for their shame and mourning those lost in the disaster, House Koryth began the cultural practice of wearing funerary masks whilst in public, a practice which survives to this day, though in a different form. These masks were once uniform, but it is now a common practice for the people of House Koryth to customise and decorate their masks to reflect their personality and personal tastes. It is also growing ever more common to see members of the House wearing half masks, or simple eye coverings in place of the traditional full masks, especially when travelling abroad.

To outsiders, the people of the house are often seen as machiavellian schemers, and Korythians are often employed by other Houses as spies and political advisors. They are also renowned for their arcane mastery, being home to Prismhall Academy, the premier school for the arcane arts in Escuria. Many of those with a gift for the arcane (whether they know it yet or not) are visited by representatives of the academy when they come of age, and being a prismatic mage is considered both a badge of honour and a warning. Unfortunately, those who aren’t blessed with magical gifts in House Koryth are often relegated to the most menial of tasks, with little room for improving their own station.

House Koryth is experiencing something of a cultural renaissance since the ascension of their current ruler, Princess Sabine Koryth, as she has begun to loosen a great deal of the societal restrictions placed upon her people in an attempt to show a more welcoming face to the rest of Escuria, and The Summer Palace has become a hotspot for socialites, bon vivants and the well-to-do of the elite in recent years. Sabine’s marriage to Admiral Mikael Tithos-Koryth even spells a potential union between House Koryth and House Tithos in the not too distant future, union which could birth a new superpower.

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```