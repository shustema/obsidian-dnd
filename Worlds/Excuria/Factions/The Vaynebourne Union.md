---
world: excuria
type: [location, faction]
location:
---
## Overview
### Ideologies
- Study
- Meritocracy
- Experimentation
## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Cities",
  contains(type, "capital") as "Capital?"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND !contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "npc")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
## History
The average Escurian citizen knows very little, if anything, about the Vaynebourne Union. This is for two reasons; firstly, they are inherently insular and don’t generally like to engage with people from outside the Union. Secondly, since their arrival in Escuria a century ago they have been locked in a constant battle for survival against the extremely aggressive House Varyn, who view them as invaders on their land.

The Vaynebourne Union were originally a literal union from the surface made up of the workers from an arcane drilling company, who formed to bring an end to the bad working conditions and low pay that their bosses enforced upon them. After unionising they managed to buy out the company and began to build their most ambitious project yet - an unimaginably large experimental arcane drill named “The Underhalle”. Their plan was to travel the material plane collecting rare resources before returning to the surface to trade, and the drill was designed to be a fully functional mobile city in which the workers and their families could live as they travelled the plane. To achieve this, they reorganised the Union to be a strict meritocracy, where the brightest and smartest would be put in positions of power to get the project done.

This worked wonders initially, leading to innovations never before seen anywhere on the plane and soon enough The Underhalle was ready for its first run. However, on its first run those in control were so keen to test the limits of the machine that they went far deeper than they had originally intended and discovered an unspeakable horror that threatened to destroy them whole, a being they refer to only as “the gnawing darkness”. They fled from this and unfortunately, instead of re-emerging on the surface, they tunnelled into a mountain range within the borders of House Varyn. Varyn took this as a sign of aggression and launched an attack, severely damaging the drill and stranding the Union in Escuria, battling for their lives.

For the past century Vaynebourne have been using their understanding of advanced arcanotech to hold Varyn at bay, but their leaders have grown stubborn and refuse to accept that the Underhalle is beyond repair. As the new generations are growing up within the Underhalle, there is a schism beginning to form between those who want to continue trying to repair the drill with their limited resources, and those who want to cannibalise it to give them a better chance of defending themselves against House Varyn. This schism has been made all the worse by the fact that the original leadership have been able to maintain their hold on power through experimental life-extending technologies, and refuse to make room for others around the debate table.

As the slow war of attrition continues to build, there are those who are looking elsewhere in Escuria for new allies, and only time will tell whether the Vaynebourne Union will continue to fight a losing battle to return home, or whether they will enter a new age here in Escuria.

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```