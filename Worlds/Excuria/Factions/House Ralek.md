---
world: excuria
type: [location, faction]
location:
---
## Overview
### Ideologies
- Zealotry
- Surveillance
- Superstition
## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Cities",
  contains(type, "capital") as "Capital?"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND !contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "npc")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
## History
House Ralek was once the breadbasket of Escuria, taking the lead in domesticating the wild animals of the isles to act as beasts of burden and farm creatures. To this day they have a strong connection to the wildlife of Escuria, favouring various types of giant spider that they have bred for specific purposes. For a time Ralek was considered the most peaceful and Idyllic of the Houses, however that was not to last.

The eruption of the volcano Chokemount on their northern border devastated large swaths of their most fertile farmland and claimed the lives of many of the nobility at the time. It was during this dark time that Prince Ralek assumed responsibility of leading the church of Lolth from his exiled sister Princess Koryth, and he began to restructure their society around this new focus, proclaiming that from that point forward the head of the church would now also function as head of the House, moving the centre of the faith to his palace at Kel’Voss.

The nobility almost immediately began to take up high positions in the priesthood as Prince Ralek undertook significant reforms of the faith with a focus on hunting down and rooting out heresy wherever it may be found. Whilst this was initially interpreted only as those who refuse to worship Lolth, the definition of heresy in modern day House Ralek is much more malleable than was probably intended at the time, now giving the House carte blanche to murder anyone who disagrees with them.

It wasn’t long before the House faced challenge in the form of House Ledilesi, who not only threatened an encroachment on their southern borders, but were also ruled over by an open heretic who claimed to be a divine being themselves. Many amongst the house believed that their failure to bring this heresy to heel was due to a lack of divine mandate on the part of Prince Ralek, and that this was proof that Lolth no longer favoured him as ruler of the House. After a successful assassination and coup, the church and the princedom passed to another high ranking priest, setting a violent precedent for the future of House Ralek. Nowadays, if you are able to scheme and murder your way into a position of power it is seen as a blessing from Lolth, and if you are the victim of one of these plots you are considered to have lost her favour and thus not to be worthy to hold your position in the first place.

This has of course led to the House becoming more and more fractured and paranoid both of those within their borders and without over the years, and this paranoia has infected the whole society. House Ralek is now a heavily monitored surveillance state constantly on the lookout for any signs of “heresy”. If the house were ever to be able to agree on what exactly that “heresy” is they could become a true force to be reckoned with, but for now they remain an insular group in constant turmoil due to their religious factionalism.

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```