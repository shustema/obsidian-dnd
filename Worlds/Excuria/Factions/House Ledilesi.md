---
world: excuria
type: [location, faction]
location:
---
## Overview
### Ideologies
- Flare
- Passion
- Spectacle
## Summaries
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Cities",
  contains(type, "capital") as "Capital?"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "Sub Locations"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "location")
  AND !contains(type, "city")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
```dataview

TABLE WITHOUT ID
  link(file.link, title) as "NPC's"
	
WHERE world = lower(split(file.folder,"/")[1])
  AND contains(type, "npc")
  AND contains(faction, lower(this.file.name))

SORT file.name ASC
```
## History
House Ledilesi has perhaps the strangest tale of any of the Noble Houses, as not only did they not start out as a recognised Noble House like those that signed the Accords of Old, but they also originated from somewhere outside of Escuria. The landmass that now comprises House Ledilesi did not used to exist, until one day it simply appeared fully formed on the border of House Marevo and House Ralek complete with towns, people and a full tropical rainforest that seems to survive despite the lack of sunlight. Those who were sent to investigate quickly reported that this landmass was known as “Ledilesi” by the inhabitants, and that their ruler went by the title of The Phoenix Empress, and claimed to be a fire god sent to bring light to the darkness.

Seeing this sudden arrival and blatant heresy as an attack both Marevo and Ralek summoned their forces in an attempt to force these invaders out, but found themselves unable to handle the unfamiliar terrain and animals of this tropical rainforest, and repelled by a force who had a fanatical devotion to the Phoenix Empress. In the end, to avoid a combined attack by the forces of Ledilesi and their newfound allies in House Tithos the remaining houses were forced to sue for peace and begrudgingly formally recognise House Ledilesi as an equal. To this day House Ledilesi and House Tithos have a strange camaraderie with one another, and Ledilesan ships are much less likely to be targeted by pirates on the Escurian seas.

House Ledilesi have made good use of their extensive natural resources that are hard to come by in Escuria, and as such their capital city of Taveo has become a hub for rarities, luxuries and oddities that may seem mundane to those on the surface, but are exotic to those native to the underdark. Their supernatural tropical climate and high material wealth have also made Taveo a popular tourist destination for the wealthy nobles who can often be found frequenting the gambling houses and attending prize fights at the arena. The same nobles who loudly decry the Phoenix Empress as a heretic often keep holiday homes in Taveo and gladly accept invites to her lavish galas and events.

The Phoenix Empress herself has all the airs and graces you would expect of a royal, however she is known to have little patience for those who defy her wishes. Any worship of a god other than her within Ledilesi is likely to see you added to the pyres atop the great beacons of Taveo, and anyone found to be causing trouble on the streets of the capital is likely to disappear without a trace. Whilst those who live in Taveo generally have one of the highest standards of living in Escuria, it is always under the watchful eye of The Phoenix Empress and her royal guard The Reborn.

## Mentions
```dataview

TABLE WITHOUT ID
    link(file.link, title) as "Session",
    campaign as "Campaign"

FROM #session-notes 
WHERE contains(this.file.inlinks, file.link)

SORT date desc
```